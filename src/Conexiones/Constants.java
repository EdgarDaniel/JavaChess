package Conexiones;

public interface Constants {
	
	int CLIENT_DESCONECTED=-2;
	int WRITE_FILE=0;
	int CODIFICAR=1;
	int FROM=2;
	int FROM_TO=3;
	int CHANGE_PROMOTION=4;
	int NOTIFICACION=5;
	int ACEPTADO=6;
	int RECHAZADO=7;
}
