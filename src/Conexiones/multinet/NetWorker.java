package Conexiones.multinet;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Conexiones.Action;
import Conexiones.ConectionListener;
import Conexiones.Constants;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class NetWorker extends JFrame implements Action,ConectionListener, Constants{

	private JPanel contentPane;
	private Client cliente;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try {
					NetWorker frame = new NetWorker();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});	   
	}

	/**
	 * Create the frame.
	 */
	public NetWorker() 
	{
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		
		addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				cliente.clm.closeConnection();
				System.exit(0);
			}
		});
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				write();
			}
		});
		panel.add(btnEnviar);
		
		cliente = new Client();
		conectar();
		
	}
	
	public void write()  
	{
		try { 

			File archivo = new File("/home/yeo/Documentos/PROYECTO BASE DE DATOS.docx");

			int tamaoArchivo = (int) archivo.length();

			FileInputStream fis = new FileInputStream(archivo);
			BufferedInputStream bis = new BufferedInputStream(fis);

			byte[] buffer = new byte[tamaoArchivo];

			bis.read(buffer);

			bis.close();
			fis.close();
			
			cliente.clm.write(WRITE_FILE, buffer, true);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		
	}
	
	public void conectar()
	{
		cliente.conectarCliente("localhost", 5230, this, this,this);
	}

	@Override
	public void action(int key, Object code) 
	{
		
		switch(key)
		{
			case WRITE_FILE:
				try 
				{
					FileOutputStream fos = new FileOutputStream("/home/yeo/Escritorio/recibido.docx");
					BufferedOutputStream out = new BufferedOutputStream(fos);
		
					byte buffer[] = (byte[]) code;
					out.write(buffer);
					fos.close();
					out.close();
		
				} 
				catch (IOException e) 
				{
					
				}
			break;
			
			case CLIENT_DESCONECTED:
				JOptionPane.showMessageDialog(this, code.toString());
			
			
		}
		
		
	}
	
	
	@Override
	public void onConected() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(this, "Te has conectado");
	}
	
	@Override
	public void onDesconected() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(this, "Conexion finalizada");
	}
	
	
	
	
}
