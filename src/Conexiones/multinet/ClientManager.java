package Conexiones.multinet;

import java.awt.Component;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.JOptionPane;
import javax.swing.Timer;

import Conexiones.Action;
import Conexiones.ConectionListener;

public class ClientManager implements Runnable
{
    private ObjectInputStream ObjectInput;

    private ObjectOutputStream ObjectOutput;

    private Action action;
    
    private ConectionListener cl;
    
    private boolean aceptado;
    
    private Timer timer;
    private int seg,max=5;
    
    private Socket socket;
    
    private Component comp;
    
    public ClientManager(Socket socket,Action ac,ConectionListener cl,Component c)
    {
    	this.socket = socket;
    	comp = c;
    	this.cl = cl;
    	action=ac;
    	
    	timer = new Timer(1000, (a)->{
    			seg++;
    			
    			if(!aceptado)
    			{
    				System.out.println("Cerrando en conexion en "+seg+"/"+max);
    				
    				if(seg==max)
    				{
    					timer.stop();
    					try {
							this.socket.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
    				}
    			}else {
    				
    				timer.stop();
    			}
    	}
    	);
    	
        try
        {
        	timer.start();
            ObjectOutput = new ObjectOutputStream(this.socket.getOutputStream());
            ObjectInput = new ObjectInputStream(this.socket.getInputStream());
            aceptado=true;
            Thread hilo = new Thread(this);
            hilo.start();
            
        } catch (Exception e)
        {
        	JOptionPane.showMessageDialog(null, "Se cancelara la conexion del cliente ");
        	System.exit(0);
        }
    }

    public void write(int key,Object code,boolean sendSelf)
    {
    	if(key==-1 || key==-2)
    	{
    		JOptionPane.showMessageDialog(comp, "Las constantes '{-1,-2}' no pueden ser usadas como un key-valor");
    		return;
    	}
    	
    	try {
			ObjectOutput.writeObject(key);
			ObjectOutput.writeBoolean(sendSelf);
	    	ObjectOutput.writeObject(code);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    /**
     * blockConnection: writeObject(-1);
     * 
     * Activa/Desactiva el bloqueo para que usuarios puedan conectarse
     */
    public void blockConnection()
    {
    	try {
			ObjectOutput.writeObject(-1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void closeConnection()
    {
    	try {
			ObjectOutput.writeObject(-2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void run()
    {
        try
        {
        	int key;
        	Object code;
        	cl.onConected();
        	
            while (true)
            {
            	key = ObjectInput.readInt();
            	code = ObjectInput.readObject();
            	
            
            	action.action(key, code);
                
            }
        } catch (Exception e)
        {
        	cl.onDesconected();
        }
    }
}
