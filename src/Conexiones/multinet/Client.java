package Conexiones.multinet;

import java.awt.Component;
import java.io.IOException;
import java.net.Socket;

import Conexiones.Action;
import Conexiones.ConectionListener;

public class Client{
	
	private Socket socket;

	public ClientManager clm;

	public void conectarCliente(String host, int puerto, Action ac,ConectionListener cl,Component c) 
	{
		try {
			socket = new Socket(host, puerto);
			clm = new ClientManager(socket, ac, cl,c);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
}
