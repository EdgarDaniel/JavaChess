package Conexiones.net;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JFrame;

import Conexiones.Action;
import Conexiones.ConectionListener;

import java.io.IOException;

public abstract class TCP extends Thread implements ConectionListener
{
	public Object code;
	public int key;
	protected ObjectInputStream entra;
	protected ObjectOutputStream sale;
	private ConectionListener onConected;
	protected Action action;
	protected JFrame parent;


	public TCP(Action ac,ConectionListener onc,String name,JFrame par)
	{
		action = ac;
		onConected = onc;
		parent = par;
		setName(name);
	}
	
	public void write(int key,Object code)
	{
		try
		{
			sale.writeObject(key);
			sale.writeObject(code);

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void onConected() 
	{
		onConected.onConected();
	}
	
	@Override
	public void onDesconected() 
	{
		onConected.onDesconected();
	}
	
	public abstract boolean connect(String host,int port);
	public abstract void close();
}