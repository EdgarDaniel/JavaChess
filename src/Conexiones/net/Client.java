package Conexiones.net;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import Conexiones.Action;
import Conexiones.ConectionListener;

public class Client extends TCP
{
	 Socket s;
	 public static final String name="client";

	public Client(Action ac,ConectionListener cl,JFrame pa)
	{
		super(ac,cl,name,pa);
	}
	  
	@Override
	public boolean connect(String host, int puerto) 
	{
        try {
			s = new Socket (host, puerto);
		    start();
		    return true;
		       
		} catch (IOException e) 
        {
			JOptionPane.showMessageDialog(super.parent, e.getMessage(),"Error de conexion",
					JOptionPane.ERROR_MESSAGE);
		}
        
        return false;
  
	}
	 
	  @Override
	public void run() 
	{
		  
		  try{
			  sale = new ObjectOutputStream(s.getOutputStream());
			  entra = new ObjectInputStream(s.getInputStream());
			  
			  onConected();
			  
	            while(true)
	            {
	            	key = (int)entra.readObject();
	                code = entra.readObject();
	                action.action(key,code);
	            }
	            
	        }catch(Exception e)
		  	{
	        	onDesconected();
	        }
	}
	  
	 @Override
	public void close() {
		// TODO Auto-generated method stub
		 
		try {
			onDesconected();
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}