package Conexiones.net;

import javax.swing.JFrame;

import Conexiones.Action;
import Conexiones.ConectionListener;

public class ConexionLocal 
{
	public int puerto;
	public String host;
	
	public ConexionLocal(String h,int p) 
	{
		host = h;
		puerto = p;
	}
	
	public TCP get(boolean server, Action action, ConectionListener onC,JFrame parent,WaiteConnection w)
	{
		return server ? new Server(action,onC,parent,w) : new Client(action,onC,parent);
	}
}
