package Conexiones.net;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import javax.swing.*;
import javax.swing.plaf.LayerUI;
 
public class BarraEspera implements WaiteConnection
{
 
  private JLayer<JPanel> jlayer;
  private Timer stopper;
  private WaitLayerUI layerUI;
  private Server server;
  private boolean conectado;
  
  
  public void waitClient(Server s,JFrame frame) {
	  
	server = s;
    JDialog f = new JDialog(frame);
    
    layerUI = new WaitLayerUI();
    JPanel panel = createPanel();
    jlayer = new JLayer<JPanel>(panel, layerUI);
     
    stopper = new Timer(1000, new ActionListener() {
      public void actionPerformed(ActionEvent ae) {
    	  
    	 if(conectado)
    	 {
    		 layerUI.stop();
    		 stopper.stop();
    		 f.dispose();
    	 }
      }
    });

    
    f.add (jlayer);
    f.setSize(300, 300);
    f.setModal(true);
    f.setDefaultCloseOperation (JDialog.DISPOSE_ON_CLOSE);
    f.setLocationRelativeTo (frame);
    
    layerUI.start();
    if (!stopper.isRunning()) {
      stopper.start();
    }
    
    f.addWindowListener(new WindowAdapter() 
    {
    	@Override
    	public void windowOpened(WindowEvent e) {
    		// TODO Auto-generated method stub
    		new SwingWorker<Void, Void>() {
    			@Override
    			protected Void doInBackground() throws Exception {
    				// TODO Auto-generated method stub
    				try {
    					server.s = server.ss.accept();
    					conectado=true;
    				} catch (IOException e1) {
    					// TODO Auto-generated catch block
    					 layerUI.stop();
    		    		 stopper.stop();
    				}
    	    		return null;
    			}
			}.execute();
    	}
	});
    
    
    
    f.setVisible (true);
    
  }
  
 
  private JPanel createPanel() {
    JPanel p = new JPanel();
 
    p.add(new JLabel("Esperando a otro jugador..."));
 
    return p;
  }
}
 
@SuppressWarnings("serial")
class WaitLayerUI extends LayerUI<JPanel> implements ActionListener {
  private boolean mIsRunning;
  private boolean mIsFadingOut;
  private Timer mTimer;
 
  private int mAngle;
  private int mFadeCount;
  private int mFadeLimit = 15;
 
  @Override
  public void paint (Graphics g, JComponent c) {
    int w = c.getWidth();
    int h = c.getHeight();
 
    // Paint the view.
    super.paint (g, c);
 
    if (!mIsRunning) {
      return;
    }
 
    Graphics2D g2 = (Graphics2D)g.create();
 
    float fade = (float)mFadeCount / (float)mFadeLimit;
    // Gray it out.
    Composite urComposite = g2.getComposite();
    g2.setComposite(AlphaComposite.getInstance(
        AlphaComposite.SRC_OVER, .5f * fade));
    g2.fillRect(0, 0, w, h);
    g2.setComposite(urComposite);
 
    // Paint the wait indicator.
    int s = Math.min(w, h) / 10;
    int cx = w / 2;
    int cy = h / 2;
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setStroke(
        new BasicStroke(s / 4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
    g2.setPaint(Color.green);
    g2.rotate(Math.PI * mAngle / 180, cx, cy);
    for (int i = 0; i < 12; i++) {
      float scale = (11.0f - (float)i) / 11.0f;
      g2.drawLine(cx + s, cy, cx + s * 2, cy);
      g2.rotate(-Math.PI / 6, cx, cy);
      g2.setComposite(AlphaComposite.getInstance(
          AlphaComposite.SRC_OVER, scale * fade));
    }
 
    g2.dispose();
  }
 
  public void actionPerformed(ActionEvent e) {
    if (mIsRunning) {
      firePropertyChange("tick", 0, 1);
      mAngle += 3;
      if (mAngle >= 360) {
        mAngle = 0;
      }
      if (mIsFadingOut) {
        if (--mFadeCount == 0) {
          mIsRunning = false;
          mTimer.stop();
        }
      }
      else if (mFadeCount < mFadeLimit) {
        mFadeCount++;
      }
    }
  }
 
  public void start() {
    if (mIsRunning) {
      return;
    }
     
    // Run a thread for animation.
    mIsRunning = true;
    mIsFadingOut = false;
    mFadeCount = 0;
    int fps = 24;
    int tick = 1000 / fps;
    mTimer = new Timer(tick, this);
    mTimer.start();
  }
 
  public void stop() {
    mIsFadingOut = true;
  }
 
  @Override
  public void applyPropertyChange(PropertyChangeEvent pce, JLayer<? extends JPanel> l) {
    if ("tick".equals(pce.getPropertyName())) {
      l.repaint();
    }
  }
}