package Conexiones.net;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import Conexiones.Action;
import Conexiones.ConectionListener;

public class Server extends TCP
{
	private WaiteConnection wait;
	public static final String name="server"; 
	
        public Server(Action ac,ConectionListener on,JFrame paren, WaiteConnection wait) 
	{
		super(ac,on,name,paren);
		
		this.wait = (wait==null) ? new BarraEspera() : wait;
			
	}

	public ServerSocket ss;
	public Socket s;
	 
	
	@Override
	public boolean connect(String host, int puerto) 
	{
		try{
            
            ss = new ServerSocket (puerto);
            wait.waitClient(this,super.parent);
            
            if(s == null) {
            	JOptionPane.showMessageDialog(super.parent, "Se ha cancelado la conexion");
            	ss.close();
            	return false;
            }
                        
            start();
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(super.parent,e.getMessage(),"Error de conexion",
					JOptionPane.ERROR_MESSAGE);
			
        }
		
		return true;
	}
	 
	 @Override
	public void run() 
	{
			try{
				
				sale = new ObjectOutputStream(s.getOutputStream());
				entra = new ObjectInputStream(s.getInputStream());
				onConected();
				
	            
	            while(true)
	            {
	            	key = (int)entra.readObject();
	                code = entra.readObject();
	                action.action(key,code);
	            }
	            
	        }
			catch (EOFException e){
				close();
				
	        } catch(SocketException e) {
	        	
	        	close();
	        }
			catch (IOException e) {
				// TODO Auto-generated catch block
				close();
	        } catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
	        	close();
			}			
	}
	 
	 @Override
	public void close() 
	{
		 try 
			{
			 
			 	onDesconected();
				ss.close();
				s.close();
				
			} 
			catch (IOException e1) 
		 	{
				e1.printStackTrace();
			}
	}

}

