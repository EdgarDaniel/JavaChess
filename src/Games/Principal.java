package Games;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Robot;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import Games.Abstract.TableroViewer;
import Games.Ajedrez.Diseno.PanelAjedrez;
import Games.Ajedrez.Perfomance.GuardaJugadas;
import Manejador.Serializa;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Color;

import static Games.ColorManager.colorFuerte;

@SuppressWarnings("serial")

public class Principal extends JFrame 
{

	private JPanel contentPane;

	private PanelAjedrez panelAjedrez;

	public static void main(String[] arg) 
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run() {
				try {

					
					
					if(arg.length==0)
					{
						Principal frame = new Principal();
						frame.setVisible(true);
//
					}
					if(arg.length==1)
					{
						System.out.println(arg[0]);
						
						if(arg[0].endsWith(".jpp"))
						{
							GuardaJugadas jugadas = (GuardaJugadas) Serializa.writeObject
									(new File(arg[0]));
							new Principal(jugadas);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public static Principal instance;
	
	public Principal() 
	{
		
		setBackground(colorFuerte);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		



		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		setSize(Toolkit.getDefaultToolkit().getScreenSize());
//		setSize(900,700);
		setResizable(false);
		if(panelAjedrez == null)
		{
			panelAjedrez = new PanelAjedrez();
		}
		
		getContentPane().add(new JScrollPane(panelAjedrez));
		
		instance = this;
	}	
	
	public Principal(GuardaJugadas jugadas)
	{
		new TableroViewer(jugadas, true);
	}
}
