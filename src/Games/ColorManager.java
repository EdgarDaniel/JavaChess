package Games;

import java.awt.Color;

public class ColorManager 
{
//	public static Color colorFuerte = Color.DARK_GRAY;
//	public static Color colorClaro = Color.LIGHT_GRAY;
//	public static Color colorMedio = Color.gray;
	
	public static Color foreground = Color.white;
	
	public static Color colorFuerte = new Color(0x5a4d2f);//Color.DARK_GRAY;
	public static Color colorClaro = new Color(0xc2ad7d);//Color.LIGHT_GRAY;	
	public static Color colorMedio = new Color(0xab8838);//Color.gray;
	 
//	public static Color colorFuerte = new Color(0x76120e);
//	public static Color colorClaro = new Color(0xc24b46);
//	public static Color colorMedio = new Color(0xb82721);
}
