package Games.Abstract;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import Games.Ajedrez.Diseno.PanelAjedrez;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


import static Games.ColorManager.colorClaro;

@SuppressWarnings("serial")
public class PanelConnect extends JPanel 
{
	
	public boolean escondido=true;
	public JLabel label;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textField;
	private JRadioButton rdbtnSinConexion;
	
	public byte modoJuego;
	
	public PanelConnect(VoidMethod onClick) 
	{
		modoJuego = Variables.MODO_NORMAL;
		
		setBackground(colorClaro);
		setLayout(null);
		
		label = new JLabel(Variables.localnet);
		
		label.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseEntered(MouseEvent e) 
			{
				label.setCursor(Variables.handCursor);
			}
			
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				onClick.method();
				escondido=!escondido;
			}
		});
		
		label.setOpaque(false);
		label.setBounds(12, 12, 41, 43);
		add(label);
		
		JLabel lblConectarseComo = new JLabel("Conectarse como:");
		lblConectarseComo.setForeground(Color.BLACK);
		lblConectarseComo.setBounds(65, 12, 116, 15);
		add(lblConectarseComo);
		
		JRadioButton rdbtnServidor = new JRadioButton("Servidor");
		rdbtnServidor.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) 
			{
				if(arg0.getStateChange() == ItemEvent.SELECTED)
					modoJuego = Variables.MODO_SERVER;
			}
		});
		buttonGroup.add(rdbtnServidor);
		rdbtnServidor.setSelected(true);
		rdbtnServidor.setForeground(Color.BLACK);
		rdbtnServidor.setOpaque(false);
		rdbtnServidor.setBounds(179, 35, 75, 23);
		add(rdbtnServidor);
		
		JRadioButton rdbtnCliente = new JRadioButton("Cliente");
		rdbtnCliente.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange() == ItemEvent.SELECTED)
					modoJuego = Variables.MODO_CLIENT;
			}
		});
		buttonGroup.add(rdbtnCliente);
		rdbtnCliente.setOpaque(false);
		rdbtnCliente.setForeground(Color.BLACK);
		rdbtnCliente.setBounds(258, 35, 75, 23);
		add(rdbtnCliente);
		
		textField = new JTextField();
		textField.setBounds(384, 12, 124, 19);
		add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton(Variables.refresh);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(modoJuego!=Variables.MODO_NORMAL)
					PanelAjedrez.tableroAjedrez.connect(modoJuego);
				else {
					PanelAjedrez.tableroAjedrez.modoJuego = modoJuego;
					PanelAjedrez.tableroAjedrez.modoConexion=false;
				}
			}
		});
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(421, 40, 41, 23);
		add(btnNewButton);
		
		JLabel lblPuerto = new JLabel("Puerto");
		lblPuerto.setHorizontalAlignment(SwingConstants.CENTER);
		lblPuerto.setForeground(Color.BLACK);
		lblPuerto.setBounds(320, 12, 57, 15);
		add(lblPuerto);
		
		rdbtnSinConexion = new JRadioButton("Sin conexion");
		rdbtnSinConexion.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) 
			{
				if(arg0.getStateChange() == ItemEvent.SELECTED)
					modoJuego = Variables.MODO_NORMAL;
			}
		});
		buttonGroup.add(rdbtnSinConexion);
		rdbtnSinConexion.setSelected(true);
		rdbtnSinConexion.setOpaque(false);
		rdbtnSinConexion.setForeground(Color.BLACK);
		rdbtnSinConexion.setBounds(67, 35, 102, 23);
		add(rdbtnSinConexion);
		
		setPreferredSize(new Dimension(520, 75));
	}
	
	public void selectNoConection()
	{
		rdbtnSinConexion.setSelected(true);
	}
	
	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return 520;
	}
	
	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return 75;
	}
}
