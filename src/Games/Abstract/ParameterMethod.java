package Games.Abstract;

public interface ParameterMethod<T> {

	void get(T obj);
}
