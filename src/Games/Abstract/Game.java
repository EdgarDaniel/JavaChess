package Games.Abstract;

import java.io.File;

public interface Game 
{
	void juegoNuevo(byte variante,Object[] modoVS,byte turno);
	void cargarJuego(File file);
	void guardarJuego(File file);
	void guardarJugadas(File file);
	boolean connect(byte modoJ);
}
