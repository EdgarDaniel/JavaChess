package Games.Abstract;

import java.awt.Color;

import javax.swing.JLabel;

@SuppressWarnings("serial")
public abstract class Casilla extends JLabel 
{
	public final byte fila,columna;
	private Color colorCasilla;
	public boolean isValid;
	
	public Casilla(byte f, byte c, Color color) 
	{
		fila = f;
		columna = c;
		colorCasilla = color;
		setOpaque(true);
		setBackground(color);
		setBorder(Variables.bordeCasillas);
		setHorizontalAlignment(CENTER);
	}
	
	public void defaultColor()
	{
		setBackground(colorCasilla);
	}
	
	public void validateCasilla(boolean v)
	{
		isValid=v;
		
		if(v)
			marck();
		else 
			defaultColor();
	}
	
	public abstract void select();
	public abstract void marck(); 
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return fila+"-"+columna;
	}
}
