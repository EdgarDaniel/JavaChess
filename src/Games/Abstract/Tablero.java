package Games.Abstract;

import static Games.Abstract.Variables.iFor;
import static Games.Abstract.Variables.jFor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JPanel;

import Manejador.DragDropListener;
import Manejador.DropFiles;


public abstract class Tablero extends JPanel implements Game
{

	private static final long serialVersionUID = 3653183218781865085L;
	public boolean pintado;
	public Color firstColor,secondColor;
	public byte filas,columnas;
	
	public Tablero(byte f, byte c, Color c1, Color c2) 
	{
		filas = f;
		columnas = c;
		pintado=true;
		setLayout(new BorderLayout(10,10));
		firstColor = c1;
		secondColor = c2;
		this.setLayout(new GridLayout(f,c,3,3));
		
		crearAtributos();
		
		for(iFor=0; iFor<f; iFor++)
		{
			for(jFor=0; jFor<c; jFor++)
			{
				instanciar(iFor, jFor);
				pintado=!pintado;
			}
			
			pintado=!pintado;
		}
		
		Variables.reserIJ();
	}
	
	public void addDraggDropListener(DragDropListener dd)
	{
		new DropFiles(this,dd);
	}
	
	public abstract void reset(); 
	public abstract void crearAtributos();
	public abstract void instanciar(byte i,byte j);
	public abstract Casilla getCasilla(int i,int j);
}


