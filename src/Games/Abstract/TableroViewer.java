package Games.Abstract;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import Games.NodeGame;
import Games.Ajedrez.Perfomance.GuardaJugadas;
import javax.swing.JSlider;


public class TableroViewer extends JPanel implements ActionListener
{
	private static final long serialVersionUID = 2641493113413964293L;

	private JLabel jlabels[][];
	
	private int delay=1000;
	private Timer timer;
	private JSlider slider;
	private GuardaJugadas jugadas;
	private JPanel juegoPanel;
	private NodeGame<PositionIcon[],Boolean> nodo;
	
	public TableroViewer(GuardaJugadas jugadas, boolean showInJFrame) 
	{
		int f = jugadas.estadoInicial.length;
		int c = jugadas.estadoInicial[0].length;
		
		this.jugadas = jugadas;
		
//		setLayout(new GridLayout(f,c,3,3));
		juegoPanel = new JPanel();
		juegoPanel.setLayout(new GridLayout(8,8,3,3));
		
		boolean x=true;
		
		jlabels = new JLabel[f][c];
		
		for(int i=0; i<f; i++)
		{
			for(int j=0; j<c; j++)
			{
				jlabels[i][j] = new JLabel();
				jlabels[i][j].setOpaque(true);
				jlabels[i][j].setHorizontalAlignment(SwingConstants.CENTER);
				jlabels[i][j].setBorder(Variables.bordeCasillas);
				jlabels[i][j].setBackground(x ? jugadas.c1 : jugadas.c2);
				x = !x;
				if(jugadas.estadoInicial[i][j]!=null)
					jlabels[i][j].setIcon(jugadas.estadoInicial[i][j].pieza.getPieza
							(jugadas.estadoInicial[i][j].valor));
				
				juegoPanel.add(jlabels[i][j]);
			}
			
			x = !x;
		}
		
		if(showInJFrame) 
		{
			JFrame frame = new JFrame();
			
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setSize(500, 500);
			frame.setLocationRelativeTo(null);
			frame.getContentPane().add(this);
			frame.setVisible(true);
		}
		else {
			JDialog frame = new JDialog();
			
			frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			frame.setSize(500, 500);
			frame.setModal(true);
			frame.setLocationRelativeTo(null);
			frame.getContentPane().add(this);
			frame.setVisible(true);
		}
		
		timer = new Timer(delay, this);
		
		JPanel panel = new JPanel();
		setLayout(new BorderLayout(0, 0));
		add(juegoPanel,"Center");
		add(panel,"South");
		
		slider = new JSlider();
		panel.add(slider);
		timer.start();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		if(jugadas.lista.hasNext())
		{
			nodo = jugadas.lista.next();
			
			actualizar();
			
			if(jugadas.lista.getNext()!=null && jugadas.lista.getNext().key)
			{
				nodo = jugadas.lista.next();
				
				if(nodo.value.length==2)
					actualizar();
				
				else {
					jlabels[nodo.value[0].fila][nodo.value[0].columna].setIcon(nodo.value[0].icon);
					System.out.println("ss");
				}
			}
		}
		else timer.stop();
	}
	
	private  void actualizar()
	{
		jlabels[nodo.value[0].fila][nodo.value[0].columna].setIcon(null);
		jlabels[nodo.value[1].fila][nodo.value[1].columna].setIcon(nodo.value[1].icon);
	}
}
