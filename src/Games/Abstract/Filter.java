package Games.Abstract;

public interface Filter<T,V>
{
	T filter(V value);
}
