package Games.Abstract;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.border.Border;

import Games.ColorManager;


public class Variables 
{
	public static Border bordeCasillas = BorderFactory.createLineBorder(Color.black);
	
	public static Cursor select = Toolkit.getDefaultToolkit().createCustomCursor(getImage("/", "select.png").getImage(),
								new Point(10,10),"WILL"),
			handCursor = new Cursor(Cursor.HAND_CURSOR),
			moveCursor = new Cursor(Cursor.MOVE_CURSOR);

	static {
		UIManager.put("OptionPane.background", ColorManager.colorFuerte);
		UIManager.put("Panel.background", ColorManager.colorFuerte);
		UIManager.put("Button.background", ColorManager.colorMedio);
	}
	
	//public static String path = Variables.class.getResource("/").getPath();

	public static ImageIcon localnet = getImage("/", "localnet.png"),
					variantesAje = getImage("/", "board-games.png"),
					resetGame = getImage("/", "reset.png"),
					refresh = getImage("/", "refresh.png"),
					modoChess = getImage("/", "modochess.png"),
					conected = getImage("/", "conected.png"),
					updateParti = getImage("/", "actP.png");
			//Icon rootIcon = getImage("/", "root.png"),
//			chess = getImage("/", "chess"),
//			damas = getImage("/", "damas.png"),
//			videos = getImage("/", "videos.png"),
//			play = getImage("/", "ver.png"),
//			partidas = getImage("/", "archivo.png"),
//			cargarPart = getImage("/", "play.png"),
//			fichero = getImage("/", "fich.png"),
//			pack = getImage("/", "package.png"),
//			pieces = getImage("/", "piezas.png");
	
	
	public static final byte JvsJ = 1, JvsPC = 2, MODO_SERVER = 3, MODO_CLIENT = 4,MODO_NORMAL=5;
								
	
	public static byte iFor,jFor;
	
	public static void reserIJ()
	{
		iFor = jFor = 0;
	}
	
	public static void assertIJFor()
	{
		if( iFor!=0 || jFor != 0)
			throw new RuntimeException("Los valores de i_jFor no estan en 0");
	}
	
	public static byte[] convertArray(String arr[])
	{
		byte x[] = new byte[arr.length];
		
		for(int i=0; i<x.length; i++)
			x[i] = Byte.parseByte(arr[i]);
		
		return x;
	}
	
	public static ImageIcon getImage(String carpeta, String file)
	{
		return new ImageIcon(Variables.class.getResource("/Recursos"+carpeta+file));
	}
}
