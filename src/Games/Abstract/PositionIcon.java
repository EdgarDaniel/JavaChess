package Games.Abstract;


import javax.swing.Icon;

import Games.Ajedrez.Perfomance.Position;

public class PositionIcon extends Position
{
	private static final long serialVersionUID = -3496148272600047468L;
	
	public Icon icon;
	
	public PositionIcon(int f, int c,Icon img) 
	{
		super(f,c);
		icon = img;
	}
}