package Games.Abstract;

public interface BooleanMethod {
	
	boolean getBool();
}
