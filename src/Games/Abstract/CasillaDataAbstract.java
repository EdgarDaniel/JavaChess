package Games.Abstract;

import java.io.Serializable;

import Games.Ajedrez.Perfomance.Pieza;

public final class CasillaDataAbstract implements Serializable
{
	private static final long serialVersionUID = 5370096659650105832L;
	public byte valor;
	public Pieza pieza;
	
	public CasillaDataAbstract(byte  valor, Pieza pieza) 
	{
		this.valor = valor;
		this.pieza = pieza;
	}
	
	
}
