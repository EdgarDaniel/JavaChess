package Games;


import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import static Games.ColorManager.colorFuerte;
import static Games.ColorManager.foreground;

public class MyOptionPane 
{
	private static JTextArea txtMensaje;
	
	static
	{
		txtMensaje = new JTextArea(5,5);
		txtMensaje.setBackground(colorFuerte);
		txtMensaje.setForeground(foreground);
		txtMensaje.setEditable(false);
	}
	
	
	public static int showConfirm(String m,String title)
	{
		txtMensaje.setText(m);
		return JOptionPane.showConfirmDialog(null, txtMensaje,title,JOptionPane.OK_CANCEL_OPTION);
	}
	
	public static void showMessage(String m,String title)
	{
		txtMensaje.setText(m);
		JOptionPane.showMessageDialog(null, txtMensaje,title,JOptionPane.INFORMATION_MESSAGE);
	}
}
