package Games;

import java.io.Serializable;

import Games.Ajedrez.Perfomance.CasillaData;

public class Codificador implements Serializable 
{

	private static final long serialVersionUID = 7412009732351256940L;

	public String[][] tableroCodificado;
	public CasillaData[][] casillas;
	
	public Codificador(String[][] tablero) 
	{
		tableroCodificado = tablero;
	}
	
	public Codificador(CasillaData[][] casillas)
	{
		this.casillas = casillas;
	}
	
}
