package Games;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import Games.Abstract.VoidMethod;

public class Animador implements ActionListener
{
	private Timer timer;
	
	private Component comp;
	
	private int maxValor,inc_DecX,x,inc_DecY,y,width,height;
	
	private VoidMethod vm;
	
	public Animador(int delay,VoidMethod onfinish) 
	{
		timer = new Timer(delay, this);
		vm = onfinish;
	}

	public Animador setFunction(Component comp,int max, int inc_decX,int inc_decY, int x, int y,int w, int h)
	{
		maxValor = max;
		this.inc_DecX = inc_decX;
		this.inc_DecY = inc_decY;
		this.comp = comp;
		this.x = x;
		this.y = y; 
		this.width = w;
		this.height = h;
		return this;
	}
	
	public void play()
	{
		timer.start();
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		x+=inc_DecX;
		y+=inc_DecY;
		
		comp.setLocation(x, y);
		
		if(inc_DecX<0)
		{
			if(x<=maxValor)
			{
			timer.stop();
			vm.method();
			}
		}
		else
		{
			if(x>=maxValor)
			{
			timer.stop();
			vm.method();
			}
		}
	}
}
