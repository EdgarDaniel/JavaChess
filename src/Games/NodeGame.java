package Games;

import java.io.Serializable;

public class NodeGame<T,K> implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2919314285734195002L;
	public NodeGame<T,K> next,before;
	public T value;
	public K key;
	
	public NodeGame(K key,T n) 
	{
		this.key = key;
		value = n;
	}

	@Override
	public String toString() 
	{
		return "("+value.toString()+ (key!= null ? key.toString()+")" : ")");
	}
}
