package Games.Ajedrez.Perfomance;

import java.awt.Color;
import java.io.Serializable;

import Games.GameList;
import Games.Abstract.CasillaDataAbstract;
import Games.Abstract.PositionIcon;

public class GuardaJugadas implements Serializable 
{
	private static final long serialVersionUID = -8635822828256774505L;
	public Color c1,c2;
	public CasillaDataAbstract[][] estadoInicial;
	public GameList<PositionIcon[],Boolean> lista; 
	
	public GuardaJugadas(CasillaDataAbstract casillas[][],Color color1,Color color2) 
	{
		estadoInicial = casillas;
		//= new CasillaDataAbstract[casillas.length][casillas[0].length];
		
//		for(int i=0; i<casillas.length; i++)
//		{
//			for(int j=0; j<casillas[0].length; j++)
//			{
//				try {
//					estadoInicial[i][j] = (CasillaDataAbstract) casillas[i][j].clone();
//				} catch (CloneNotSupportedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
		
		c1 = color1;
		c2 = color2;
		lista = new GameList<>();
	}
	
	public void addJugada(PositionIcon...pos)
	{
		lista.insert(false,pos);
	}
	
	public void addJugadaDoble(PositionIcon...pos)
	{
		lista.insert(true,pos);
	}
	
//	public Position[] obtenerAnterior()
//	{
//		return lista.before().nodos;
//	}
	
}
