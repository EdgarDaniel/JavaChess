package Games.Ajedrez.Perfomance;

import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;
import static Games.Ajedrez.Perfomance.VarsChess.JUGADOR_BLANCAS;
import static Games.Ajedrez.Perfomance.VarsChess.JUGADOR_NEGRAS;
import static Games.Ajedrez.Perfomance.VarsChess.turno;

import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.Timer;

import Games.GameList;
import Games.MyOptionPane;
import Games.NodeGame;
import Games.Principal;
import Games.Abstract.BooleanMethod;
import Games.Abstract.Filter;
import Games.Abstract.MyAnot;
import Games.Abstract.VoidMethod;


public class PC
{

	/*
	 * Princípiante: Todo es aleatorio, no importa donde mene, puede ser buena jugada, mala jugada, 
	 * 					o no ayuda en nada la pieza meneada
	 * 
	 * 		
	 * Normal: El proceso sigue siendo casi aleatorio,con la diferencia de que al defender o atacar
	 * decidira cual es la mejor opcion, para esto evaluara:
	 * DEfensa:
	 * 		* Si hay una pieza a defender, checara si al situarse en defensa, es comida por un enemigo
	 * 		(si no guardar esta opcion)y evaluara el valor de la pieza a perder y de haber varias en defensa, evaluara todas
	 * 		y escogera la pieza de menor valor para arriesgar esa.
	 * 	
	 * 		Si hay varias piezas a defender, checara cual pieza es de mayor valor y la defendera
	 * 		Ademas si al defender puede ser comida por una pieza enemiga, checar si hay mas 
	 * 		piezas que se puedan poner a la defensa y escoger la de menor valor
	 * 
	 * Ataque:
	 *      Si hay una pieza rival para atacar, checar el valor de la pieza a comer y almacenar el valor,
	 *      y ademas checar si al atacar se es comida por una pieza rival (si no guadar esta opcion
	 *      y si hay una en defesa determinar que hacer si defender o atacar (acorde al valor de las piezas)), 
	 *      y almancenar tambien el valor de la pieza que es comida (con los valores almacenados se determinara si se defefiende o se ataca
	 *      en caso de haber los dos casos). Y de existir varias piezazas que atacan a una misma pieza rival
	 *      y tambien en la defensa hay piezas enemigas que ataquen elegir la pieza de menor valor y con esa comerte la 
	 *      pieza del rival(para que te coman la de menor valor)
	 *      
	 *      Si una pieza o varias atacan a varias enemigas, checar cual pieza eneiga tiene mas valor
	 *      	y atacar con la pieza que se come dicha pieza rival,en caso de al pnerse en la defensa
	 *      una pieza rival te amenace, escoger la pieza de menor valor (si es que varias las atacan)
	 *      para arriesgar dicha pieza.
	 *      
	 * En caso de de haber tanto ataque como defensa, evealar la meor opcion, acoorde a los puntos
	 * 		 
	 * 
	 * Avanzado: Si hay varias piezas a defender, checara cual todas las piezas y evaluara si al situarse
	 * 			en defensa es comida y que valor tiene la pieza comida, una vez
	 * 			checad toas las piezas a defender y determinar con cual haces mayor puntujae tu,
	 * 			(cmer una buena ficha, pero no pierdes una ficha de tanto valor) una funcion que haga esto***
	 * 
	 * Dificil
	 */
	
	private Timer timer;
	public EstadoTablero estado;
	private PeligrosPiezas pcPeligros;
	private Position pos;
	private ArrayList<CasillaData> piezasOnGame;
	public byte turno;
	
	private Random lb = new Random();
	private VoidMethod dificultad;
	
	public PC(EstadoTablero est) 
	{
		estado = est;
		// TODO Auto-generated constructor stub
		timer = new Timer(500, (a)->{
				moverPieza();
				timer.stop();
				
			});
		
	}
	
	
	public void updateDificulty(Dificulty dif)
	{
		switch(dif)
		{
			case NOOB:
				dificultad=this::modoAletorio;
				break;
			
			case FACIL:
				dificultad = this::modoFacil;
				break;
				
			case NORMAL:
				break;
			
			case AVANZADO:
				break;
				
			case DIFICIL:
				break;
			
		}
		
	}
	
	public PC updateOptions(byte turno)
	{
		this.turno = turno;
		if(turno==JUGADOR_NEGRAS)
		{
			pcPeligros =  estado.peligrosReyN;
			piezasOnGame = estado.piezasNegras;
			pos = estado.positionRey2;
		}
		else
		{
			pcPeligros =  estado.peligroReyB;
			piezasOnGame = estado.piezasBlancas;
			pos = estado.positionRey1;
		}
		return this;
	}
	
	public void start()
	{
		VarsChess.continued=false;
		timer.start();
	}

	/*
	 * Facil: Aqui checara si se esta siendo amenezada una pieza, o bien si se esta atacando a una pieza
	 * 			y aletorioamene decidira cual opcion tomar, si comer o defender, de no haber piezas 
	 * 			por atacar o defender, el proceso es aletorio.
	 * 			Para atacar de haber varias opciones eleigira la pieza rival de mayor valor 
	 * 			Para defender de haber varias opciones defendera a la pieza amiga que tenga mayor valor
	 * 			Para atacar y defender, si hay mas de 1 pieza que puede atacar o defender a la pieza
	 * 			de mayor valor esocger aleatoriamente que pieza menear 
	 * Si hay jaques, se decicidra si se defiende o se ataca, no imortaperder la pieza
	 * 
 */
	//TODO actual
	private void modoFacil()
	{
		//Error: Cuando una pieza esta en posociones del rey (reina abajo, y su rey arriba)
		//esta pieza no la toma en cuenta para atacar
		GameList<CasillaData, CasillaData> atakes = ataques( (cas)->
		{
			if(cas.pieza.valor>valorPiezaComible)
			{
				valorPiezaComible = cas.pieza.valor;
				return REMOVE_AND_ADD;
			}
			
			if(cas.pieza.valor==valorPiezaComible)
				return ADD;
			
			return -1;
		});
		
		if(pcPeligros.piezasAtacando.isEmpty())
		{
			if(!atakes.isEmpty())
			{
				NodeGame<CasillaData, CasillaData> nodeRnd = atakes.getRandom();
				
				CasillaData from,to;
				from = nodeRnd.value;
				to = nodeRnd.key;
				
				updatePropierties(from, to);
				
				atakes=null;
			}
			
			else
			{
				if(pcPeligros.casillasPosibles.isEmpty())
				{
					moverPiezaAleatoria();
				}
				else
				{
					if(!moverReyAtacando())
					{
						int prob = lb.nextInt(10);
						
						if(prob>=3 && (piezasOnGame.size()>1) && estado.hayMovs)
							moverPiezaAleatoria();
						
						else moverReyAleatorio();
					}
				}
			}
		}
		else if(pcPeligros.piezasAtacando.getSize()==1)
		{
			easyOneJacke();
		}
		
		else if(pcPeligros.piezasAtacando.getSize()>=2)
		{
			if(!moverReyAtacando())
				moverReyAleatorio();
		}
		
			
	}
	
	
	
	private ArrayList<CasillaData> getPiezasPlayer()
	{
		return VarsChess.turno == JUGADOR_BLANCAS ? estado.piezasBlancas : estado.piezasNegras;
	}
	
	/*
	 * Cuando l rey puede comer te come(no valida si se deja en jaque)
	 */
	private GameList<CasillaData, CasillaData> ataques(Filter<Byte, CasillaData> filter)
	{
		//Cambia el turno, si es jugador de las blancas, lo cambia  a las negras, y asi evaluara
		//si las negras tienen algun oponente que las amenace
		VarsChess.turno = (byte)-turno;
		valorPiezaComible = -1;
		
		ArrayList<CasillaData> list = getPiezasPlayer();
		
		//Para T: la casilla que amenaza al rival, para  K: la casilla que es amenazda por el rival
		GameList<CasillaData, CasillaData> piezasAtacanRival = new GameList<CasillaData, CasillaData>();
		
		Amenazas.setActualNonePosition();
		list.forEach
		(
				(casRival)->
				{
					Amenazas.setOnFoundDanger( (casAtaca)->
					{
						//Cambia el turno para ver si la pieza que atacca/defiende es valida, es decir
						//no deje en jaque al rey
						if(!casAtaca.esRey())
						{
						
							VarsChess.turno=(byte) -VarsChess.turno;
							casAtaca.pieza.validarPieza(casAtaca.f, casAtaca.c);
							VarsChess.turno=(byte) -VarsChess.turno;
							
							//Aqui checar en validar por index
							
							if(casAtaca.pieza.indexEv==-1)
							{
								if(filter==null)
								{
									piezasAtacanRival.insert(casRival,casAtaca);
								}
								else
								{
									switch(filter.filter(casRival))
									{
										 case REMOVE_AND_ADD:
											 piezasAtacanRival.removeAll();
											 piezasAtacanRival.insert(casRival,casAtaca);
											 break;
											 
										 case ADD:
											 piezasAtacanRival.insert(casRival,casAtaca);
											 break;
									
									}
								}
							}
							else
							{
								//Si la pieza puede comer, pero deja en jaque al rey, solo 
								//movera si la pieza esta en la misma direccion (es decir, que comera
								//y no dejara en jaque al rey) 
								Pieza.onFound = ()->
								{
									if(!estado.get(VarsChess.k, VarsChess.l).estaVacia())
									{
										if(!piezasAtacanRival.existsBoth(casAtaca, casRival))
											piezasAtacanRival.insert(estado.get(VarsChess.k, VarsChess.l),casAtaca);
									}
								};
								
								VarsChess.turno=(byte) -VarsChess.turno;
								if(casAtaca.pieza.puedeAtacar(casAtaca.pieza.indexEv))
								{
									casAtaca.pieza.buscarPorIndex(casAtaca.f, casAtaca.c, false);
								}
								VarsChess.turno=(byte) -VarsChess.turno;
							}
						}
					});
					
					Amenazas.buscaPiezas(casRival.f, casRival.c, VarsChess.turno, false, true, true);
				}
		);
		
		VarsChess.turno = (byte)turno;
		System.out.println("Meores piezaas: ");
		piezasAtacanRival.forEach(this::mostrarAmenazas);
		
		return piezasAtacanRival;
	}
	
	
	//TODO ataues
	private GameList<CasillaData, Void> ataquesRey(Filter<Byte, CasillaData> filter)
	{
		valorPiezaComible = -1;
		
		GameList<CasillaData, Void> piezasAtacaRey = new GameList<CasillaData, Void>();
		
		pcPeligros.casillasPosibles.forEach((nodo)->
		{
			if(!nodo.value.estaVacia())
			{
				if(filter!=null)
				{
					
					switch(filter.filter(nodo.value))
					{
					 case REMOVE_AND_ADD:
						 piezasAtacaRey.removeAll();
						 piezasAtacaRey.insert(null,nodo.value);
						 break;
						 
					 case ADD:
						 piezasAtacaRey.insert(null,nodo.value);
						 break;
					}
					
				}else piezasAtacaRey.insert(null, nodo.value);
			}
		});
		
		return piezasAtacaRey;
	}
	
	private boolean moverReyAtacando()
	{
		GameList<CasillaData, Void> posiciones = ataquesRey((cas)->
		{
			if(cas.pieza.valor>valorPiezaComible)
			{
				valorPiezaComible = cas.pieza.valor;
				return REMOVE_AND_ADD;
			}
			
			if(cas.pieza.valor==valorPiezaComible)
				return ADD;
			
			return -1;
		});
		
		//Si no hay piezas a comer, menear al rey donde sea
		if(posiciones.isEmpty())
			return false;
		
		else {
			CasillaData from,to;
			from = estado.get(pos.fila, pos.columna);
			
			to = posiciones.getRandom().value;
			
			updatePropierties(from, to);
			return true;
		}
	}
	
	private void mostrarAmenazas(NodeGame<CasillaData, CasillaData> datos)
	{
		System.out.printf("%s ataca a la pieza: %s\n",datos.value,datos.key);
	}
	
	private void defensas()
	{
		
	}
	
	//Variables usada para el ataque
	private byte valorPiezaComible;
	
	private static final byte REMOVE_AND_ADD=1,ADD=2;
	
	private void modoAletorio()
	{
//		estado = tableroAjedrez.getEstadoActual();
		
		
		if(pcPeligros.piezasAtacando.isEmpty())
		{
			if(pcPeligros.casillasPosibles.isEmpty())
			{
				moverPiezaAleatoria();
			}
			else
			{
				int prob = lb.nextInt(10);
				
				if(prob>=3 && (piezasOnGame.size()>1) && estado.hayMovs)
				{
					moverPiezaAleatoria();
				}
				
				else
				{
					moverReyAleatorio();
				}
			}
		}
		else if(pcPeligros.piezasAtacando.getSize()==1)
		{
			randomOneJacke();
		}
		
		else if(pcPeligros.piezasAtacando.getSize()>=2)
		{
			moverReyAleatorio();
		}
	}
	
	//Cuando hay un jaque todas las piezas comen la misma enemmiga,sin mebargo el rey
	//pede que se coma esa, o bien tenga otra pieza mejor que comer
	private void easyOneJacke()
	{
		boolean comer = !pcPeligros.casillasComeAtaque.isEmpty();
		boolean atacarconRey = !pcPeligros.casillasPosibles.isEmpty();
		
		Principal.instance.setTitle("Nada");
		
		if(comer && atacarconRey)
		{
			if(!moverReyAtacando())
			{
				CasillaData from,to;
				
				from = pcPeligros.casillasComeAtaque.getRandom().value;
				
				to = pcPeligros.piezasAtacando.getNext().value;
				
				updatePropierties(from, to);
				
			}
			
			return;
		}
		else if(comer)
		{
			CasillaData from,to;
			
			from = pcPeligros.casillasComeAtaque.getRandom().value;
			
			to = pcPeligros.piezasAtacando.getNext().value;
			
			updatePropierties(from, to);
			return;
		}
		else if(atacarconRey)
		{
			if(!moverReyAtacando())
			{
				if(pcPeligros.casillasCubreAtaque.isEmpty())
				{
					Principal.instance.setTitle("Elr rey no puede ser cubridos");
					moverReyAleatorio();
					return;
				}
				else 
				{
					int x = lb.nextInt(2);
					
					
					if(x==0)
					{
						moverReyAleatorio();
						return;
					}
				}
			}else return;
		}
			
		CasillaData from,to;
		Principal.instance.setTitle(":"+pcPeligros.casillasCubreAtaque+"");
//		System.out.println(pcPeligros.casillasPosibles.getSize());
//		System.out.println("CCA.size = "+pcPeligros.casillasCubreAtaque.getSize());
		NodeGame<CasillaData, CasillaData> n = pcPeligros.casillasCubreAtaque.getRandom();
		from = n.value;
		to = n.key;
		updatePropierties(from, to);
		
		
	}
	
	private void randomOneJacke()
	{
		//Cubrir,comer,mover rey
		ArrayList<Integer> vals = new ArrayList<Integer>();
		CasillaData from=null,to=null;
		
		if(!pcPeligros.casillasCubreAtaque.isEmpty())
		{
			vals.add(1);
		}
		
		if(!pcPeligros.casillasComeAtaque.isEmpty())
		{
			vals.add(2);
		}
		
		if(!pcPeligros.casillasPosibles.isEmpty())
		{
			vals.add(3);
		}
		
		int valor = vals.get(lb.nextInt(vals.size()));

		switch(valor)
		{
			case 1:
				NodeGame<CasillaData, CasillaData> n = pcPeligros.casillasCubreAtaque.getRandom();
				from = n.value;
				to = n.key;
				break;
				
			case 2:
				from = pcPeligros.casillasComeAtaque.getRandom().value;
				to = pcPeligros.piezasAtacando.getNext().value;
				break;
				
			case 3:
				from = estado.get(pos.fila, pos.columna);
				
				to = pcPeligros.casillasPosibles.getRandom().value;
				break;
		}
		
		updatePropierties(from, to);
	}
	
	private void moverReyAleatorio()
	{
		CasillaData from=null,to=null;
		
		from = estado.get(pos.fila, pos.columna);
		
		to = pcPeligros.casillasPosibles.getRandom().value;
		
		updatePropierties(from, to);
	}
	
	private void moverPiezaAleatoria()
	{
		CasillaData from=null,to=null;
		from = obtenerPiezaAleatoria();
		to = from.movimientos.getRandom().value;
		updatePropierties(from, to);
	}
	
	private CasillaData obtenerPiezaAleatoria()
	{
		boolean vacio=false;
		CasillaData from;
		
		do
		{
			int index = lb.nextInt(piezasOnGame.size());
			
			from = piezasOnGame.get(index);
			
			vacio = from.movimientos.isEmpty();
		}
		while(vacio);
		
		return from;
	}
	
	private void updatePropierties(CasillaData from,CasillaData to)
	{
		
		VarsChess.piezaEnJuego = from.pieza;
		tableroAjedrez.casillasAj[from.f][from.c].setIcon(null);
		tableroAjedrez.casillasAj[to.f][to.c].putPiece(from.pieza, turno);
		Propiedades.copyPropierties(from.f, from.c, to.f, to.c);
		tableroAjedrez.update();
		VarsChess.continued=true;
		VarsChess.turno = (byte) -turno;
		
	}
	
	public void moverPieza()
	{
		VarsChess.turno = turno;
		dificultad.method();
		
	}
}

