package Games.Ajedrez.Perfomance;

import static Games.Abstract.Variables.iFor;
import static Games.Abstract.Variables.reserIJ;
import static Games.Abstract.Variables.assertIJFor;
import static Games.Ajedrez.Perfomance.VarsChess.k;
import static Games.Ajedrez.Perfomance.VarsChess.l;
import static Games.Ajedrez.Perfomance.VarsChess.movsLibre;

import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;

import javax.swing.Icon;


@SuppressWarnings("serial")
public class Reina extends Pieza
{

	public Reina(byte val)
	{
		super("Reina","D",val);
	}
	
	@Override
	public Icon getPieza(byte pla) 
	{
		
		if(pla==VarsChess.JUGADOR_BLANCAS)
			return VarsChess.REINA;
		
		else if(pla==VarsChess.JUGADOR_NEGRAS)
			return VarsChess.REINA2;
		
		return  null;
	}
	
	@Override
	public boolean puedeAtacar(int orientation) {
		// TODO Auto-generated method stub
		return true;
	}
	
	
	@Override
	public void buscaCaminos(byte f, byte c) 
	{
		
		super.buscaCaminos(f, c);
		
		if(indexEv==-1)
		{
			assertIJFor();
			
			for(iFor = 0; iFor<movsLibre.length; iFor++)
			{
				try
				{
					k = (byte) (f+movsLibre[iFor][0]);
					l = (byte) (c+movsLibre[iFor][1]);
					
					
					while(true)
					{
						if(esEnemigo(k, l))
						{
							onFound.method();
							break;
						}
						
						if(tableroAjedrez.getEstadoActual().get(k,l).estaVacia())
						{
							onFound.method();
							k += movsLibre[iFor][0];
							l += movsLibre[iFor][1];
						}
						
						else break;
					}
					
				}
				catch(ArrayIndexOutOfBoundsException ex) {
					
				}
			}
			reserIJ();
		}
		else
		{
			buscarPorIndex(f, c,true);
		}
	}
}
