package Games.Ajedrez.Perfomance;

import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;
import static Games.Ajedrez.Perfomance.VarsChess.JUGADOR_BLANCAS;
import static Games.Ajedrez.Perfomance.VarsChess.alfil;
import static Games.Ajedrez.Perfomance.VarsChess.movsCaballo;
import static Games.Ajedrez.Perfomance.VarsChess.movsLibre;
import static Games.Ajedrez.Perfomance.VarsChess.torre;

import Games.GameList;
import Games.Abstract.BooleanMethod;
import Games.Abstract.ParameterMethod;

public class Amenazas 
{
	private static BooleanMethod metodo;
	private static byte index,filaAct,colAct,incF;

	private static CasillaData casilla;
	
	public static void breakOnFoundDanger()
	{
		metodo = () -> true;
	}
	
	
	public static void setOnFoundDanger(ParameterMethod<CasillaData> vm)
	{
		metodo = () -> {
			vm.get(casilla);
			return false;
		};
	}
	
	public static void setActualPosition(byte f, byte c)
	{
		filaAct = f;
		colAct = c;
	}
	
	public static void setActualNonePosition()
	{
		filaAct = colAct = -1;
	}
	
	public static boolean buscaPiezas(int f, int c, int player, boolean breakAfound, boolean enemigas,boolean peonDiagonal)
	{
		if(enemigas)
		{
			player=-player;
			incF = (byte) (player==JUGADOR_BLANCAS ? 1 : -1);
		}else incF = (byte) (player==JUGADOR_BLANCAS ? -1 : 1);
		
		if(amenazaCaballo(f, c, player) && breakAfound)
				return true;
		
		return amenazaOtra(f, c, player,peonDiagonal);
	}
	
	private static boolean amenazaOtra(int f, int c, int player,boolean peonDiag)
	{
		Pieza aux = torre;
		int m,n;
		boolean enemigo=false;
		
		//Aqui el peon es usado para cuando puede comer en diagonal
		if(peonDiag)
		{
			if(tableroAjedrez.getEstadoActual().isValidPosition(f+incF, c+1))
			{
				if((casilla = tableroAjedrez.getEstadoActual().get(f+incF, c+1)).esPeonDe(player))
				{
					enemigo=true;
					if(metodo.getBool())
					{
						return true;
					}
				}
			}
			
			if(tableroAjedrez.getEstadoActual().isValidPosition(f+incF, c-1))
			{
				if((casilla = tableroAjedrez.getEstadoActual().get(f+incF, c-1)).esPeonDe(player))
				{
					enemigo=true;
					if(metodo.getBool())
					{
						return true;
					}
				}
			}
		}
		//Para cubrir casillas
		else
		{
			//Peon que cubre avanzando solo una casilla
			if(tableroAjedrez.getEstadoActual().isValidPosition(f+incF, c))
			{
				if((casilla = tableroAjedrez.getEstadoActual().get(f+incF, c)).esPeonDe(player))
				{
					enemigo=true;
					
					if(metodo.getBool())
						return true;
				}
				
				//Peon quee puede cubrir por avanazar dos casillas
				else if(tableroAjedrez.getEstadoActual().isValidPosition(f+incF+incF, c))
				{
					if(tableroAjedrez.getEstadoActual().get(f+incF, c).estaVacia())
						if((casilla = tableroAjedrez.getEstadoActual().get(f+incF+incF, c)).esPeonDe(player))
						{
							if(player==JUGADOR_BLANCAS)
							{
								if((f+incF+incF) == tableroAjedrez.filas-2)
								{
									enemigo = true;
									
									if(metodo.getBool())
										return true;
								}
							}
							else
							{
								if((f+incF+incF) == 1)
								{
									enemigo = true;
									
									if(metodo.getBool())
										return true;
								}
							}
						}
				}
			}
		}
		
		for(index=0; index<movsLibre.length; index++)
		{
			if(index==4)
				aux = alfil;
			
			m = f+movsLibre[index][0];
			n = c+movsLibre[index][1];
			
			if(!tableroAjedrez.getEstadoActual().isValidPosition(m, n))
				continue;
			
			if((casilla = tableroAjedrez.getEstadoActual().tablero[m][n]).esReyDe(player))
			{
				if(!enemigo)
					enemigo=true;
				
				if(metodo.getBool())
				{
					return true;
				}
				
				continue;
			}
			
			
				while(true)
				{
					if(!tableroAjedrez.getEstadoActual().isValidPosition(m, n))
						break;
					
					if(tableroAjedrez.getEstadoActual().tablero[m][n].estaVacia() || (m==filaAct && n==colAct))
					{
						m += movsLibre[index][0];
						n += movsLibre[index][1];
						continue;
					}
					
					else if((casilla = tableroAjedrez.getEstadoActual().tablero[m][n]).esDamaDe(player) ||
							tableroAjedrez.getEstadoActual().tablero[m][n].esPiezaDe(aux, player))
					{
						if(!enemigo)
							enemigo=true;
						
						if(metodo.getBool())
						{
							return true;
						}
					
						else break;
					}
					
					break;
				}
		}
		
		return enemigo;
	}
	
	public static byte determinarOrientacion(int fromF,int fromC,int toF,int toC)
	{
		byte index=-1;
		
		//Estan en la misma fila
		if(fromF == toF)
			index = (fromC<toC) ? VarsChess.DERECHA : VarsChess.IZQUIERDA;

		//Estan en la misma columna
		else if(fromC == toC)
			index = (fromF<toF) ? VarsChess.ABAJO: VarsChess.ARRIBA;

		
		else if(fromF-fromC == toF - toC)
			index = (fromF < toF) ? VarsChess.ABAJO_DER : VarsChess.ARRIBA_IZQ;
		
		else if(fromF+fromC == toF+toC)
			index = (fromF < toF) ? VarsChess.ABAJO_IZQ: VarsChess.ARRIBA_DER;
		
		return index;
	}
	
	public static void agregaCasillasAmenaza(int fromF,int fromC,int toF,int toC, 
												GameList<CasillaData, Void> casillasRef)
	{
		//Determina la forma de buscar las casillla que amenazan, si estan a la derecha
		//tomar como referencia los movsAladerecha y asi
		
		byte index = determinarOrientacion(fromF, fromC, toF, toC);
		
		while(true)
		{
			fromF+=movsLibre[index][0];
			fromC+=movsLibre[index][1];
			
			if(!(fromF==toF && fromC==toC))
			{
				casillasRef.insert(null, tableroAjedrez.getEstadoActual().get(fromF, fromC));
				continue;
			}
			break;
		}
	}
	
	public static boolean amenazaCaballo(int f, int c, int player)
	{
		int m,n;
		boolean enemigo=false;
		
		for(index=0; index<movsCaballo.length; index++)
		{
			m = f+movsCaballo[index][0];
			n = c+movsCaballo[index][1]; 
			
			if(!tableroAjedrez.getEstadoActual().isValidPosition(m, n))
				continue;
			
			if((casilla = tableroAjedrez.getEstadoActual().tablero[m][n]).esCaballoDe(player))
			{
				if(!enemigo)
					enemigo=true;
				
				if(metodo.getBool())
					return true;
			}
		}
		return enemigo;
	}
}
