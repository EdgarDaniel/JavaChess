package Games.Ajedrez.Perfomance;

import java.io.Serializable;

public class Position implements Serializable,Cloneable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 289775722862895596L;
	public int fila,columna;
	
	public Position() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Position clone(){
		// TODO Auto-generated method stub
		try {
			return (Position) super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Position(int f, int c) 
	{
		fila = f;
		columna = c;
	}
	
	public void setNewPosition(int f, int c) 
	{
		fila = f;
		columna = c;
	}
	
	@Override
	public String toString() 
	{
		return String.format("[%d][%d]", fila,columna);
	}
}
