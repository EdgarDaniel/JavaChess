package Games.Ajedrez.Perfomance;

import javax.swing.Icon;

import static Games.Abstract.Variables.iFor;
import static Games.Abstract.Variables.reserIJ;
import static Games.Abstract.Variables.assertIJFor;

import static Games.Ajedrez.Perfomance.VarsChess.k;
import static Games.Ajedrez.Perfomance.VarsChess.l;
import static Games.Ajedrez.Perfomance.VarsChess.movsLibre;
import static Games.Ajedrez.Perfomance.VarsChess.ABAJO_DER;
import static Games.Ajedrez.Perfomance.VarsChess.ABAJO_IZQ;
import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;

@SuppressWarnings("serial")
public class Alfil extends Pieza 
{
	public Alfil(byte val) 
	{
		super("alfil","A",val);
	}
	
	@Override
	public Icon getPieza(byte pla) 
	{ 
		if(pla==VarsChess.JUGADOR_BLANCAS)
			return VarsChess.ALFIL;
		
		else if(pla==VarsChess.JUGADOR_NEGRAS)
			return VarsChess.ALFIL2;

		return null;
	}
	
	@Override
	public boolean puedeAtacar(int orientation) 
	{
		return orientation>=ABAJO_DER && orientation <=ABAJO_IZQ;
	}
	
	@Override
	public void buscaCaminos(byte f, byte c) 
	{
		super.buscaCaminos(f, c);

		if(indexEv==-1)
		{
			assertIJFor();
			
			for(iFor = 4; iFor<movsLibre.length; iFor++)
			{
				try
				{
					k = (byte) (f+movsLibre[iFor][0]);
					l = (byte) (c+movsLibre[iFor][1]);
					
					while(true)
					{
						if(esEnemigo(k, l))
						{
							onFound.method();
							break;
						}
						
						if(tableroAjedrez.getEstadoActual().get(k,l).estaVacia())
						{
							onFound.method();
							k += movsLibre[iFor][0];
							l += movsLibre[iFor][1];
						}
						
						else break;
					}
					
				}
				catch(ArrayIndexOutOfBoundsException ex) {
					
				}
			}
			
			reserIJ();
		}
		else
		{
			if(puedeAtacar(indexEv))
				buscarPorIndex(f, c,true);
		}
	}
}
