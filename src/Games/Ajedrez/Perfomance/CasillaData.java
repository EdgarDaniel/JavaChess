package Games.Ajedrez.Perfomance;


import java.io.Serializable;

import Games.GameList;
import Games.Abstract.Especial;
import Games.Abstract.ParameterMethod;

import static Games.Ajedrez.Perfomance.VarsChess.JUGADOR_BLANCAS;
import static Games.Ajedrez.Perfomance.VarsChess.JUGADOR_NEGRAS;
import static Games.Ajedrez.Perfomance.VarsChess.peon;
import static Games.Ajedrez.Perfomance.VarsChess.rey;
import static Games.Ajedrez.Perfomance.VarsChess.reina;
import static Games.Ajedrez.Perfomance.VarsChess.alfil;
import static Games.Ajedrez.Perfomance.VarsChess.torre;
import static Games.Ajedrez.Perfomance.VarsChess.caballo;
import static Games.Ajedrez.Perfomance.VarsChess.k;
import static Games.Ajedrez.Perfomance.VarsChess.l;

import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;

public class CasillaData  implements Serializable, Cloneable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4679405141721151553L;
	public byte id_color;
	public Especial especialAct;
	
	public String casillaName;
	public final byte f,c;
	public byte valor;
	public transient Pieza pieza;
	public GameList<CasillaData, Void> movimientos;

	public CasillaData(byte f , byte c,byte id)
	{
		this.f = f;
		this.c = c;
		casillaName = "--";
		valor = -1;
		id_color=id;
		movimientos = new GameList<CasillaData, Void>();
		
	}
	
	public void actualizarMovs()
	{
		movimientos.removeAll();
		
		if(pieza!=rey)
		{
			Pieza.onFound = () -> {
				movimientos.insert(null, tableroAjedrez.getEstadoActual().tablero[k][l]);
				
				if(tableroAjedrez.getEstadoActual().checarMovs)
					if(!tableroAjedrez.getEstadoActual().hayMovs)
						tableroAjedrez.getEstadoActual().hayMovs=true;
				
			};
			pieza.buscaCaminos(f, c);
		}
	}
	
	public void onClick(ParameterMethod<CasillaData> metodo)
	{
		if(!movimientos.isEmpty())
		{
			while(movimientos.hasNext())
			{
				metodo.get(movimientos.next().value);
			}
		}
	}
	
	public void actualizar(byte val, Pieza piece)
	{
		this.valor=val;
		pieza=piece;
		
		if(valor == JUGADOR_BLANCAS)
			casillaName = piece.clave+"B";
		
		else if(valor == JUGADOR_NEGRAS)
			casillaName = piece.clave+"N";
		
		else casillaName="--";
		
	}
	
	
	public boolean estaVacia()
	{
		return valor == -1;
	}
	
	
	public boolean esBlancas()
	{
		
		return valor == JUGADOR_BLANCAS;
	}
	
	public boolean esNegras()
	{
		return valor == JUGADOR_NEGRAS;
	}
	
	public boolean esPiezaDe(Pieza p, int py)
	{
		return pieza == p && valor == py;
	}
	
	public boolean esReyDe(int player)
	{
		return pieza == rey && valor == player;
	}
	
	public boolean esRey()
	{
		return pieza==rey;
	}
	
	public boolean esDamaDe(int player)
	{
		return pieza == reina && valor == player;
	}
	
	public boolean esCaballoDe(int player)
	{
		return pieza == caballo && valor==player;
	}
	
	public boolean esAlfil()
	{
		return pieza == alfil;
	}
	
	public boolean esPeonDe(int player)
	{
		return pieza == peon && valor == player;
	}
	
	public boolean esTorre()
	{
		return pieza == torre;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException 
	{
		CasillaData clon;
		
		clon = (CasillaData) super.clone();
		
		return clon;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format("%s: [%d]-[%d] = %s\n", pieza,f,c,(especialAct==null) ? "N" : "Y");
	}
}
