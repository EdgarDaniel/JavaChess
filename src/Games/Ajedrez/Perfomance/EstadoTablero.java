package Games.Ajedrez.Perfomance;


import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Games.MyOptionPane;
import Games.Abstract.ParameterMethod;
import Games.Abstract.PositionIcon;

import static Games.Ajedrez.Perfomance.VarsChess.JUGADOR_BLANCAS;
import static Games.Ajedrez.Perfomance.VarsChess.JUGADOR_NEGRAS;
import static Games.Ajedrez.Perfomance.VarsChess.alfil;
import static Games.Ajedrez.Perfomance.VarsChess.caballo;
import static Games.Ajedrez.Perfomance.VarsChess.torre;

import static Games.Abstract.Variables.iFor;
import static Games.Abstract.Variables.jFor;
import static Games.Abstract.Variables.assertIJFor;
import static Games.Abstract.Variables.reserIJ;


import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;


public class EstadoTablero implements Cloneable, Serializable
{

	private static final long serialVersionUID = 1306415675973277815L;

	public CasillaData tablero[][];
	
	public Position positionRey1,positionRey2;
	
	public ArrayList<CasillaData> piezasBlancas,piezasNegras;
	
	public PeligrosPiezas peligroReyB,peligrosReyN;
	
	public CasillaData peonPaso,peonIzq,peonDer;
	
	public byte f,c;
	
	public transient Pieza coronacionNegras,coronacionBlancas; 
	
	private transient boolean comio;
	public transient boolean reseteando,hayMovs,checarMovs;
	public boolean enroqueN=true,enroqueB=true,actionOnPanel,deleteAction;
	
	public EstadoTablero(byte f,byte c)
	{
		tablero = new CasillaData[f][c];
		coronacionBlancas = VarsChess.reina;
		coronacionNegras = VarsChess.reina;
		
		peligroReyB = new PeligrosPiezas("Rey blanco");
		peligrosReyN = new PeligrosPiezas("Rey negro");
		
		actionOnPanel = true;
		this.f = f;
		this.c = c;
		assertIJFor();
		boolean x=true;
		for(; iFor<f; iFor++)
		{
			for(jFor=0;jFor<c; jFor++)
			{
				tablero[iFor][jFor] = new CasillaData(iFor, jFor,  (byte) (x ? 1 : 2));
				x=!x;
			}
			x=!x;
		}
		
		jugadasEspeciales();
		positionRey1 = new Position();
		positionRey2 = new Position();
		piezasBlancas = new ArrayList<CasillaData>(16);
		piezasNegras = new ArrayList<CasillaData>(16);
		
		reserIJ();
	}
	
	@SuppressWarnings("unchecked")
	private EstadoTablero(CasillaData tablero[][],Position posR1,Position posR2, ArrayList<CasillaData> piezaN,
			ArrayList<CasillaData> piezaB,boolean enroqueBla,boolean enroqueNeg,byte fi,byte col,
			PeligrosPiezas pelRb,PeligrosPiezas pelRn,byte turno) 
	{
		this.tablero = new CasillaData[tablero.length][tablero[0].length];
		f = fi;
		c = col;
		actionOnPanel = false;
		enroqueB = enroqueBla;
		enroqueN = enroqueNeg;
		
		for(int i=0; i<tablero.length; i++)
		{
			for(int j=0; j<tablero[0].length; j++)
			{
				try {
					this.tablero[i][j] = (CasillaData) tablero[i][j].clone();
				} catch (CloneNotSupportedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		
		this.positionRey1=posR1.clone();
		this.positionRey2=posR2.clone();
		piezasNegras = (ArrayList<CasillaData>) piezaN.clone();
		piezasBlancas=(ArrayList<CasillaData>) piezaB.clone();
		this.peligroReyB = pelRb.copy();
		this.peligrosReyN = pelRn.copy();
		
	}
	
	public void setCaptura(CasillaData cas)
	{
		if(cas==null)
		{
			peonPaso = null;
			
			if(peonIzq!=null)
			{
				peonIzq.especialAct = null;
				peonIzq = null;
			}
			
			if(peonDer!=null)
			{
				peonDer.especialAct = null;
				peonDer = null;
			}
		}
		
		else if(peonPaso == null)
			peonPaso = cas;
	}
	
	public EstadoTablero copy()
	{
		EstadoTablero estado = new EstadoTablero(tablero, positionRey1, positionRey2, piezasNegras, piezasBlancas,
						enroqueB,enroqueN,f,c,peligroReyB,peligrosReyN,VarsChess.turno);
		
		return estado;
	}

	private void jugadasEspeciales()
	{
		tablero[0][2].especialAct = ()->
		{
			actualizarEspecial(0, 0, 0, 3, torre, JUGADOR_NEGRAS);
			return null;
		};
		tablero[0][6].especialAct = ()->
		{
			actualizarEspecial(0, 7, 0, 5, torre, JUGADOR_NEGRAS);
			return null;
		};
		tablero[f-1][2].especialAct = ()->
		{
			actualizarEspecial(f-1, 0, f-1, 3, torre, JUGADOR_BLANCAS);
			return null;
		};
		tablero[f-1][6].especialAct = ()->
		{
			actualizarEspecial(f-1, 7, f-1, 5, torre, JUGADOR_BLANCAS);
			return null;
		};
		
	}
	
	public void actualizarEspecial(int fromF,int fromC,int toF,int toC,Pieza p,byte player)
	{
		this.actualizarTablero(fromF, fromC, player);
		
		if(toF!=-1 | toC!=-1)
		{
			this.actualizarTablero(toF, toC, p, player);
			tableroAjedrez.jugadas.addJugadaDoble(new PositionIcon(fromF, fromC,null), new PositionIcon(toF, toC,
					p.getPieza(player)));
		}
		
		else tableroAjedrez.jugadas.addJugadaDoble(new PositionIcon(fromF, fromC,null));
		
		if(actionOnPanel)
		{
			tableroAjedrez.casillasAj[fromF][fromC].setIcon(null);
			if(toF!=-1 | toC!=-1)
				tableroAjedrez.casillasAj[toF][toC].putPiece(p, player);
		}
	}
	
	public void coronacion(int f, int c,byte player,Pieza p)
	{
		this.tablero[f][c].actualizar(player, p);
		tableroAjedrez.jugadas.addJugadaDoble(new PositionIcon(f,c,p.getPieza(player)));
		
		if(actionOnPanel)
		{
			tableroAjedrez.casillasAj[f][c].putPiece(p, player);
		}
		
	}
	
	public void actualizarTablero(int f, int c,Pieza pieza,byte val)
	{
		comio=false;
		if(val == JUGADOR_BLANCAS)
		{
			piezasBlancas.add(tablero[f][c]);
			
			//Indica que las blancas se comieron una pieza
			if(tablero[f][c].pieza != null && !reseteando)
			{
				if(f == 0 && tablero[f][c].pieza==torre)
				{
					if(c==0)
					{
						tablero[0][2].especialAct=null;
						actualizarEnroques(false);
					}
					else if(c==7)
					{
						tablero[0][6].especialAct=null;
						actualizarEnroques(false);
					}
				}
				
				piezasNegras.remove(tablero[f][c]);
				comio=true;
			}
		}
		else {
			piezasNegras.add(tablero[f][c]);
			
			if(tablero[f][c].pieza != null && !reseteando)
			{
				if(f == this.f-1 && tablero[f][c].pieza==torre)
				{
					if(c==0)
					{
						tablero[f][2].especialAct=null;
						actualizarEnroques(true);
					}
					else if(c==7)
					{
						tablero[f][6].especialAct=null;
						actualizarEnroques(true);
					}
				}
				
				piezasBlancas.remove(tablero[f][c]);
				comio=true;
				
			}
		}
		
		tablero[f][c].actualizar(val, pieza);
		
		if(comio)
		{
			if(piezasBlancas.size()+piezasNegras.size()<=4)
			{
				if(piezasSobrantes(piezasNegras, piezasBlancas) || piezasSobrantes(piezasBlancas, piezasNegras))
				{
					MyOptionPane.showMessage("Insuficencia de material","Tablas");
					VarsChess.continued=false;
				}
			}
		}
	}
	

	public void actualizarTablero(int f, int c, int val)
	{
		tablero[f][c].actualizar((byte) -1, null);
		
		if(val == JUGADOR_BLANCAS)
			piezasBlancas.remove(tablero[f][c]);
		
		else if(val == -JUGADOR_BLANCAS)
			piezasNegras.remove(tablero[f][c]);
	}
	
	public boolean isValidPosition(int f, int c)
	{
		return f>=0 && f<tablero.length && c>=0 && c<tablero[0].length;
	}
	
	public CasillaData get(int f, int c)
	{
		return tablero[f][c];
	}
	
	public void reset()
	{
		
		piezasBlancas.clear();
		piezasNegras.clear();
		enroqueB = enroqueN = true;
		peligroReyB.reset();
		peligrosReyN.reset();
		jugadasEspeciales();
	}
	
	public void deleteActionOf(int f, int c)
	{
		tablero[f][c].especialAct=null;
		
	}
	
	public void actualizarEnroques(boolean blancas)
	{
		if(blancas)
			enroqueB = tablero[f-1][2].especialAct!=null || tablero[f-1][6].especialAct!=null;
		else
			enroqueN = tablero[0][2].especialAct!=null || tablero[0][6].especialAct!=null;
	}
	
	
	public void mostrar(ParameterMethod<String> metodo)
	{
		for(CasillaData x[] : tablero)
		{
			for(int i=0; i<x.length; i++)
			{
				if(i==x.length-1)
					metodo.get("\""+x[i].casillaName+"\"\n");
				
				else metodo.get("\""+x[i].casillaName+"\" ");
				
			}
			
		}
		metodo.get("\nPB: "+piezasBlancas.size());
		metodo.get("\nPN: "+piezasNegras.size());
		metodo.get("\nPosR1: "+positionRey1);
		metodo.get("\nPosR2: "+positionRey2);
		metodo.get("\nEnroqueB: "+enroqueB);
		metodo.get("\nEnroqueN: "+enroqueN);
		metodo.get("\n"+peligrosReyN);
		metodo.get("\n"+peligroReyB);
	}
	
	public boolean determinarReyAhogado(PeligrosPiezas peligros)
	{
		return peligros.casillasPosibles.isEmpty() && !hayMovs;
	}
	
	public boolean determinarJaque(PeligrosPiezas peligros)
	{
		if(peligros.piezasAtacando.getSize()==1)
		{
			return peligros.casillasComeAtaque.isEmpty() && peligros.casillasCubreAtaque.isEmpty()
					&& peligros.casillasPosibles.isEmpty();
		}
		else if(peligros.piezasAtacando.getSize()>=2)
		{
			return peligros.casillasPosibles.isEmpty();
		}
		return false;
	}
	
	
	private boolean piezasSobrantes(ArrayList<CasillaData> lista,ArrayList<CasillaData> lista2)
	{
		if(lista.size()==1 && lista2.size()==1)
		{
			return true;
		}
		
		if(lista.size()==2 && lista2.size()==1)
		{
			 return contienePieza(alfil, lista,1) || contienePieza(caballo, lista,1);
		}
		
		if(lista.size()==2 && lista2.size()==2)
		{
			return (contienePieza(alfil, lista,1) && contienePieza(alfil, lista2,1)) ||
					(contienePieza(caballo, lista,1) && contienePieza(caballo, lista2,1));
		}
		
		if(lista.size()==3 && lista2.size()==1)
		{
			return contienePieza(caballo, lista,2);
		}
		
		return false;
	}
	
	private boolean contienePieza(Pieza piece,ArrayList<CasillaData> list, int veces)
	{
		int cont=0;
		
		for(CasillaData c : list)
			if(c.pieza == piece)
				cont++;
				
		return cont==veces;
	}
}
