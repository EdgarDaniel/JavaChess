package Games.Ajedrez.Perfomance;


import javax.swing.Icon;
import javax.swing.ImageIcon;


public class VarsChess 
{

	public static final Icon REY1 = getImage("/Rey/", "rey.png"); 
	public static final ImageIcon REY2 = (ImageIcon) getImage("/Rey/", "rey2.png"); 
	
	public static final Icon ALFIL = getImage("/Alfil/", "alfil.png");
	public static final Icon ALFIL2 = getImage("/Alfil/", "alfil2.png");
	
	public static final Icon CABALLO = getImage("/Caballo/", "caballo.png");
	public static final Icon CABALLO2 = getImage("/Caballo/", "caballo2.png");
	
	public static final Icon PEON = getImage("/Peon/", "peonB.png");
	public static final Icon PEON2 = getImage("/Peon/", "peonN.png");
	
	public final static Icon REINA = getImage("/Reina/", "reina.png");
	public final static Icon REINA2 = getImage("/Reina/", "reina2.png");
	
	public static final Icon TORRE = getImage("/Torre/", "torre.png");
	public static final Icon TORRE2 = getImage("/Torre/", "torre2.png");
	
	public static final byte JUGADOR_NEGRAS = 2;
	public static final byte JUGADOR_BLANCAS = -JUGADOR_NEGRAS;
	
	public static final byte TRADICIONAL = 1, ALETORIO_FISCHER = 2, TRASCENDENTAL = 3,JUBILADO=4;
	
	public static byte turno,k,l;
	
	public static boolean continued;
	
	
	public static final byte [][] movsCaballo = 
		{
				{-2,-1},
				{-2,1},	
				{2,-1},
				{2,1},
				{-1,-2},
				{1,-2},
				{-1,2},
				{1,2}
		};
	
	public static final byte ARRIBA = 0;
	public static final byte ABAJO = 1;
	public static final byte IZQUIERDA = 2;
	public static final byte DERECHA = 3;
	public static final byte ABAJO_DER = 4;
	public static final byte ARRIBA_IZQ = 5;
	public static final byte ARRIBA_DER = 6;
	public static final byte ABAJO_IZQ = 7;
	
	public static byte getInverse(byte valor)
	{
		switch (valor) 
		{
			case ARRIBA:
				return ABAJO;
				
			case ABAJO:
				return ARRIBA;
				
			case IZQUIERDA:
				return DERECHA;
				
			case DERECHA:
				return IZQUIERDA;
				
			case ABAJO_DER:
				return ARRIBA_IZQ;
				
			case ARRIBA_IZQ:
				return ABAJO_DER;
				
			case ARRIBA_DER:
				return ABAJO_IZQ;
				
			case ABAJO_IZQ:
				return ARRIBA_DER;
	
			default:
				return -1;
		}
	}
	
//	No menear posiciones
	public final static byte[][] movsLibre =
		{
				{-1,0},//Arriba
				{1,0},//abajo
				{0,-1},//Izq
				{0,1},//Der
				
				{1,1},//Abajo-Der
				{-1,-1},//Arriba-Izq
				{-1,1},//Arriba-Der
				{1,-1},//Abajo-Izq
		};
	
	public static Reina reina = new Reina((byte) 9);
	public static Rey rey = new Rey((byte)10);
	public static Alfil alfil = new Alfil((byte)3);
	public static Torre torre = new Torre((byte)5);
	public static Caballo caballo = new Caballo((byte)3);
	public static Peon peon = new Peon((byte)1);
	
	//Cuando se escoge una pieza esta es asgindada a la variable piezaEnjuego, para
	// a la hora de actualiar el tablero todo salga bien
	public static Pieza piezaEnJuego;

	public static Icon getImage(String carpeta, String file)
	{
		return new ImageIcon(VarsChess.class.getResource("/Recursos/Ajedrez/Piezas"+carpeta+file));
	}
}
