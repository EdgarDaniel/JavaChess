package Games.Ajedrez.Perfomance;

import static Games.Abstract.Variables.iFor;
import static Games.Abstract.Variables.reserIJ;
import static Games.Abstract.Variables.assertIJFor;

import static Games.Ajedrez.Perfomance.VarsChess.movsLibre;
import static Games.Ajedrez.Perfomance.VarsChess.k;
import static Games.Ajedrez.Perfomance.VarsChess.l;
import static Games.Ajedrez.Perfomance.VarsChess.turno;

import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;

import static Games.Ajedrez.Perfomance.Amenazas.breakOnFoundDanger;
import static Games.Ajedrez.Perfomance.Amenazas.setActualPosition;
import static Games.Ajedrez.Perfomance.Amenazas.buscaPiezas;

import javax.swing.Icon;

import Games.Abstract.VoidMethod;


@SuppressWarnings("serial")
public class Rey extends Pieza
{
	private byte ff,cf;
	
	public Rey(byte val)
	{
		super("Rey","R",val);
	}

	public transient VoidMethod methodRey;
	
	@Override
	public Icon getPieza(byte pla) 
	{
		
		if(pla==VarsChess.JUGADOR_BLANCAS)
			return VarsChess.REY1;
		
		else if(pla==VarsChess.JUGADOR_NEGRAS)
			return VarsChess.REY2;
		
		return null;
	}

	public void buscaCaminos(Position pos)
	{
		buscaCaminos((byte)pos.fila, (byte)pos.columna);
	}
	
	@Override
	public boolean puedeAtacar(int orientation) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void buscaCaminos(byte f, byte c)
	{
		assertIJFor();
		
		breakOnFoundDanger();
		setActualPosition(f, c);
		
		for(iFor = 0; iFor<movsLibre.length; iFor++)
		{
			try
			{
				k = (byte) (f+movsLibre[iFor][0]);
				l = (byte) (c+movsLibre[iFor][1]);
				
				if((esEnemigo(k, l) || tableroAjedrez.getEstadoActual().get(k,l).estaVacia()))
				{
					if(!buscaPiezas(k, l, turno, true,true,true))
					{
						methodRey.method();
					}
				}
				
			}
			catch(ArrayIndexOutOfBoundsException ex) {
			}
		}
		
		ff = f;
		cf = c;
		if(turno == VarsChess.JUGADOR_BLANCAS) 
			enroque(tableroAjedrez.getEstadoActual().enroqueB,tableroAjedrez.getEstadoActual().peligroReyB,
					tableroAjedrez.filas-1);
		else
			enroque(tableroAjedrez.getEstadoActual().enroqueN,tableroAjedrez.getEstadoActual().peligrosReyN,0);
		
		reserIJ();
	}
	
	private void enroque(boolean enroque,PeligrosPiezas peligros, int f)
	{
		if(enroque && peligros.piezasAtacando.isEmpty())
		{
			if(tableroAjedrez.getEstadoActual().get(f, 2).especialAct!=null)
					enroqueLargo();
			
			if(tableroAjedrez.getEstadoActual().get(f, 6).especialAct!=null)
				enroqueCorto();
		}
	}
	
	private void enroqueLargo()
	{
		byte v;
		
		for(v = (byte) (cf-1); v>=1; v--)
		{
			if(!tableroAjedrez.getEstadoActual().get(ff, v).estaVacia() || buscaPiezas(ff, v, turno, true,true,true)) 
				return;
		}
		
		k = ff;
		l = (byte) (v+2);
		
		methodRey.method();
	}
	
	private void enroqueCorto()
	{
		byte v;
		
		for(v = (byte) (cf+1); v<tableroAjedrez.columnas-1; v++)
		{
			if(!tableroAjedrez.getEstadoActual().get(ff, v).estaVacia() || buscaPiezas(ff, v, turno, true,true,true))
				return;
		}
		
		
		k = ff;
		l = (byte) (v-1);
		
		
		methodRey.method();
	}
}
