
package Games.Ajedrez.Perfomance;

import javax.swing.Icon;

import static Games.Ajedrez.Perfomance.VarsChess.k;
import static Games.Ajedrez.Perfomance.VarsChess.l;
import static Games.Ajedrez.Perfomance.VarsChess.turno;
import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;


@SuppressWarnings("serial")
public class Peon extends Pieza 
{
	
	private byte inc,filaDoblePaso;
	
	public Peon(byte val) {
		// TODO Auto-generated constructor stub
		super("peon","P",val);
	}
	
	@Override
	public Icon getPieza(byte pl) 
	{
		if(pl==VarsChess.JUGADOR_BLANCAS)
			return VarsChess.PEON;
		
		else if(pl==VarsChess.JUGADOR_NEGRAS)
			return VarsChess.PEON2;
		
		return null;
	} 
	
	@Override
	public boolean puedeAtacar(int orientation) {
		// TODO Auto-generated method stub
		return false;
	}
	
	private void avanzarAdelante(byte f, byte c)
	{
		if(turno == VarsChess.JUGADOR_BLANCAS) 
		{
			inc = -1;
			filaDoblePaso = (byte) (tableroAjedrez.filas-4);
		}
		else 
		{			
			inc = 1;
			filaDoblePaso = 3;
		}
		
		if(tableroAjedrez.getEstadoActual().get(f+inc, c).estaVacia())
		{
			l = c;
			k = (byte) (f+inc);
			onFound.method();
			
			if(f == filaDoblePaso + (-2*inc))
				if(tableroAjedrez.getEstadoActual().get(filaDoblePaso, c).estaVacia())
				{
					k = filaDoblePaso;
					onFound.method();
				}
		}
	}

	@Override
	public void buscaCaminos(byte f, byte c)
	{
		super.buscaCaminos(f, c);
		
		if(indexEv==-1)
		{
			avanzarAdelante(f, c);
			
			if(esEnemigo(f+inc, c+1))
			{
				k = (byte) (f+inc);
				l = (byte) (c+1);
				
				onFound.method();
			}
			
			if(esEnemigo(f+inc, c-1))
			{
				k = (byte) (f+inc);
				l = (byte) (c-1);
				
				onFound.method();
			}
			
			
			if(tableroAjedrez.getEstadoActual().tablero[f][c].especialAct!=null)
			{
				Position pos = (Position) tableroAjedrez.getEstadoActual().tablero[f][c].especialAct.especial_action();
				k = (byte) pos.fila;
				l = (byte) pos.columna;
				onFound.method();
						
			}
		}
		else
		{
			int ladoColumna=0;
			
			
			if(turno == VarsChess.JUGADOR_BLANCAS)
			{
				inc = -1;
				
				switch(indexEv) 
				{
					case VarsChess.ARRIBA_IZQ:
						ladoColumna = -1;
						break;
						
					case VarsChess.ARRIBA_DER:
						ladoColumna = 1;
						break;
						
					case VarsChess.ARRIBA:
					case VarsChess.ABAJO:
						avanzarAdelante(f, c);
						return;
				}
			}
			else
			{
				inc = 1;
				
				switch(indexEv) 
				{
					case VarsChess.ABAJO_DER:
						ladoColumna = 1;
						break;
						
					case VarsChess.ABAJO_IZQ:
						ladoColumna = -1;
						break;
						
					case VarsChess.ABAJO:
					case VarsChess.ARRIBA:
						avanzarAdelante(f, c);
						return;
				}
				
			}
			
			if(ladoColumna!=0)
			{
				if(esEnemigo(f+inc, c+ladoColumna))
				{
					k = (byte) (f+inc);
					l = (byte) (c+ladoColumna);
					
					onFound.method();
				}
			}
		}
	}
}