package Games.Ajedrez.Perfomance;

import java.io.Serializable;

import Games.GameList;

public class PeligrosPiezas  implements Serializable 
{

	private static final long serialVersionUID = -1810071402863088733L;

	public  GameList<CasillaData, Void> piezasAtacando;
	
	public  GameList<CasillaData, Void> casillasPosibles;
	
	public   GameList<CasillaData, Void> casillasPasaAmenaza;
	
	public  GameList<CasillaData, Void> casillasComeAtaque;
	
	public  GameList<CasillaData, CasillaData> casillasCubreAtaque;
	
	private String name;
	
	public PeligrosPiezas(String name) 
	{
		this.name = name;
		piezasAtacando = new GameList<CasillaData, Void>();
		casillasPosibles = new GameList<CasillaData, Void>();
		casillasPasaAmenaza = new GameList<CasillaData, Void>();
		casillasComeAtaque = new GameList<CasillaData, Void>();
		casillasCubreAtaque = new GameList<CasillaData, CasillaData>();
	}
	
	public void reset()
	{
		piezasAtacando.removeAll();
		casillasPosibles.removeAll();
		casillasPasaAmenaza.removeAll();
		casillasComeAtaque.removeAll();
		casillasCubreAtaque.removeAll();
	}
	
	public PeligrosPiezas copy()
	{
		PeligrosPiezas pel = new PeligrosPiezas(name);
		
		pel.piezasAtacando = piezasAtacando.copy();
		pel.casillasPosibles = casillasPosibles.copy();
		pel.casillasPasaAmenaza = casillasPasaAmenaza.copy();
		pel.casillasComeAtaque = casillasComeAtaque.copy();
		pel.casillasCubreAtaque = casillasCubreAtaque.copy();
		return pel;
	}
	
	@Override
	public String toString() 
	{
		return String.format("Piezas atacan %s: %s\nCasillas posibles %s: %s\nCasillas Pasa Amenaza: %s\n"
				+ "Casillas como pieza atacante: %s\nCasillas cubre ataque: %s", name,piezasAtacando,name,casillasPosibles,
				casillasPasaAmenaza,casillasComeAtaque,casillasCubreAtaque);
	}
}
