package Games.Ajedrez.Perfomance;

import javax.swing.Icon;

import static Games.Abstract.Variables.iFor;
import static Games.Abstract.Variables.reserIJ;
import static Games.Abstract.Variables.assertIJFor;

import static Games.Ajedrez.Perfomance.VarsChess.k;
import static Games.Ajedrez.Perfomance.VarsChess.l;
import static Games.Ajedrez.Perfomance.VarsChess.movsCaballo;

import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;

@SuppressWarnings("serial")
public class Caballo extends Pieza
{
	public Caballo(byte val) {
		
		super("caballo","C",val);
	}
	
	@Override
	public Icon getPieza(byte pla) 
	{ 
		if(pla==VarsChess.JUGADOR_BLANCAS)
			return VarsChess.CABALLO;
		
		else if(pla==VarsChess.JUGADOR_NEGRAS)
			return VarsChess.CABALLO2;
		
		return null;
	}
	
	@Override
	public boolean puedeAtacar(int orientation) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void buscaCaminos(byte f, byte c)
	{
		super.buscaCaminos(f, c);
		
		if(indexEv==-1)
		{
			assertIJFor();
			
			for(iFor = 0; iFor<movsCaballo.length; iFor++)
			{
				try
				{
					k = (byte) (f+movsCaballo[iFor][0]);
					l = (byte) (c+movsCaballo[iFor][1]);
					
					if(tableroAjedrez.getEstadoActual().get(k,l).estaVacia() || esEnemigo(k,l))
					{
						onFound.method();
					}
					
				}
				catch(ArrayIndexOutOfBoundsException ex) {
					
				}
			}
			
			reserIJ();
		}
	}
}
