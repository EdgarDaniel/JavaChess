package Games.Ajedrez.Perfomance;

import static Games.Abstract.Variables.iFor;
import static Games.Abstract.Variables.reserIJ;
import static Games.Abstract.Variables.assertIJFor;

import static Games.Ajedrez.Perfomance.VarsChess.k;
import static Games.Ajedrez.Perfomance.VarsChess.l;
import static Games.Ajedrez.Perfomance.VarsChess.movsLibre;
import static Games.Ajedrez.Perfomance.VarsChess.ARRIBA;
import static Games.Ajedrez.Perfomance.VarsChess.DERECHA;
import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;


import javax.swing.Icon;


@SuppressWarnings("serial")
public class Torre extends Pieza
{
	public Torre(byte val) {
		// TODO Auto-generated constructor stub
		super("torre","T",val);
	}
	
	@Override
	public Icon getPieza(byte pla) 
	{ 
		
		if(pla==VarsChess.JUGADOR_BLANCAS)
			return VarsChess.TORRE;
		
		else if(pla==VarsChess.JUGADOR_NEGRAS)
			return VarsChess.TORRE2;
		
		return null;
	}
	
	@Override
	public boolean puedeAtacar(int orientation) {
		// TODO Auto-generated method stub
		return orientation>=ARRIBA && orientation<=DERECHA;
	}
	
	@Override
	public void buscaCaminos(byte f, byte c)
	{
		super.buscaCaminos(f, c);
	
		if(indexEv==-1)
		{
			assertIJFor();
			
			for(iFor = 0; iFor<4; iFor++)
			{
				try
				{
					k = (byte) (f+movsLibre[iFor][0]);
					l = (byte) (c+movsLibre[iFor][1]);
					
					while(true)
					{
						if(esEnemigo(k, l))
						{
							onFound.method();
							break;
						}
						
						if(tableroAjedrez.getEstadoActual().get(k,l).estaVacia())
						{
							onFound.method();
							k += movsLibre[iFor][0];
							l += movsLibre[iFor][1];
						}
						
						else break;
					}
					
				}
				catch(ArrayIndexOutOfBoundsException ex) {
					
				}
			}
			
			reserIJ();
		}
		else
		{
			if(puedeAtacar(indexEv))
				buscarPorIndex(f, c,true);
		}
	}
	
}