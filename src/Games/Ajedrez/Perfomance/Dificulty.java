package Games.Ajedrez.Perfomance;

public enum Dificulty 
{
	NOOB(0,"Principiante"),
	FACIL(1,"Facil"),
	NORMAL(2,"Normal"),
	AVANZADO(3,"Avanzado"),
	DIFICIL(4,"Dificil");
	
	
	private int val;
	private String name;
	
	private Dificulty(int val,String n)
	{
		this.val = val;
		name = n;
	}
	
	public int getValue()
	{
		return val;
	}
	
	public String getName()
	{
		return name;
	}
}
