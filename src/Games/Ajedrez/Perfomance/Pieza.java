package Games.Ajedrez.Perfomance;


import javax.swing.Icon;
import javax.swing.JOptionPane;

import Games.Abstract.VoidMethod;

import static Games.Ajedrez.Perfomance.VarsChess.JUGADOR_BLANCAS;
import static Games.Ajedrez.Perfomance.VarsChess.k;
import static Games.Ajedrez.Perfomance.VarsChess.l;
import static Games.Ajedrez.Perfomance.VarsChess.turno;

import java.io.Serializable;

import static Games.Ajedrez.Perfomance.VarsChess.movsLibre;
import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;

public abstract class Pieza implements Serializable
{
	
	private static final long serialVersionUID = 7902924151547252613L;
	public final String nombre,clave;
	public final byte valor;
	
	protected byte indexEv;
	
	public Pieza(String name,String clave,byte valor)
	{
		this.nombre = name;
		this.clave = clave;
		this.valor = valor;
	}
	
	public static VoidMethod onFound;
	
	public abstract Icon getPieza(byte player);
	
	public void buscaCaminos(byte f, byte c) 
	{
		validarPieza(f, c);
	}
	
	public final void validarPieza(byte f, byte c)
	{
		Position pos = (turno == JUGADOR_BLANCAS) ? tableroAjedrez.getEstadoActual().positionRey1 :
			tableroAjedrez.getEstadoActual().positionRey2;
		
		indexEv = Amenazas.determinarOrientacion(f, c,pos.fila,pos.columna);
		if(indexEv!=-1)
		{
			int a=pos.fila,b=pos.columna;
			
			indexEv = VarsChess.getInverse(indexEv);
			
			while(true)
			{
				a+=movsLibre[indexEv][0];
				b+=movsLibre[indexEv][1];
				
				if(!tableroAjedrez.getEstadoActual().isValidPosition(a, b))
				{
					indexEv=-1;
					break;
				}

				else if(tableroAjedrez.getEstadoActual().get(a,b).estaVacia() || (a == f && b == c))
					continue;
				
				if(esEnemigo(a, b))
				{
					if(tableroAjedrez.getEstadoActual().get(a, b).pieza.puedeAtacar(indexEv))
					{
//						if(Amenazas.determinarOrientacion(a, b, f, c) != indexEv)
//							JOptionPane.showMessageDialog(null, "Hay un enemigo en: "+a+"-"+b);
//						
//						else indexEv=-1;
						if(Amenazas.determinarOrientacion(a, b, f, c) == indexEv)
							indexEv=-1;
					}
					else
					{
						indexEv=-1;
					}
					break;
				}
				indexEv=-1;
				break;
			}
		}
	}

	public void buscarPorIndex(byte f, byte c,boolean both)
	{
		k = (byte) (f + movsLibre[indexEv][0]);
		l = (byte) (c + movsLibre[indexEv][1]);
		
		while(true)
		{
			if(esEnemigo(k, l))
			{
				onFound.method();
				break;
			}
			
			if(tableroAjedrez.getEstadoActual().get(k,l).estaVacia())
			{
				onFound.method();
				k += movsLibre[indexEv][0];
				l += movsLibre[indexEv][1];
				continue;
			}
			break;
		}
		
		if(both)
		{
			indexEv = VarsChess.getInverse(indexEv);
			buscarPorIndex(f, c, false);
		}
	}
	
	public abstract boolean puedeAtacar(int orientation);

	public static boolean esEnemigo(int f, int c)
	{
		try
		{
		if(tableroAjedrez.getEstadoActual().get(f, c).estaVacia())
			return false;
		
		return turno==JUGADOR_BLANCAS  ? tableroAjedrez.getEstadoActual().get(f, c).esNegras() :
					tableroAjedrez.getEstadoActual().get(f, c).esBlancas();
		}
		catch(ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return nombre;
	}
}
