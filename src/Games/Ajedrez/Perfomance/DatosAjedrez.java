package Games.Ajedrez.Perfomance;

import java.io.Serializable;

public class DatosAjedrez implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3156417887951785248L;
	public byte varianteJuego,modoComJuego,turno;
	public Object modoVS[];
	public EstadoTablero estado;

	public DatosAjedrez(byte turno,byte variante,Object modoVS[],byte modoComunication,EstadoTablero est) 
	{
		this.turno = turno;
		varianteJuego = variante;
		this.modoVS = modoVS;
		modoComJuego = modoComunication;
		estado = est;
	}
}
