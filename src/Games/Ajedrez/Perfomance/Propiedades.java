package Games.Ajedrez.Perfomance;

import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;
import static Games.Ajedrez.Perfomance.VarsChess.JUGADOR_BLANCAS;
import static Games.Ajedrez.Perfomance.VarsChess.JUGADOR_NEGRAS;
import static Games.Ajedrez.Perfomance.VarsChess.piezaEnJuego;
import static Games.Ajedrez.Perfomance.VarsChess.rey;
import static Games.Ajedrez.Perfomance.VarsChess.peon;
import static Games.Ajedrez.Perfomance.VarsChess.torre;
import static Games.Ajedrez.Perfomance.VarsChess.turno;

import javax.swing.JOptionPane;

import Games.MyOptionPane;
import Games.Abstract.PositionIcon;

public class Propiedades 
{
	private static byte fromF,fromC,toF,toC;
	private static EstadoTablero estado;
	
	public static void copyPropierties(byte fromF,byte fromC,byte toF,byte toC)
	{
		estado = tableroAjedrez.getEstadoActual();
		
		Propiedades.fromF = fromF;
		Propiedades.fromC = fromC;
		Propiedades.toF = toF;
		Propiedades.toC = toC;
		
		tableroAjedrez.jugadas.addJugada(new PositionIcon(fromF,fromC,null), new PositionIcon(toF,toC,
				piezaEnJuego.getPieza(turno)));
		
		estado.actualizarTablero(fromF, fromC,turno);
		estado.actualizarTablero(toF, toC, piezaEnJuego, turno);
		
		estado.deleteAction = true;
		
		if(piezaEnJuego == torre)
			actualizaEnroque();
		
		else if(piezaEnJuego == rey)
			actualizarRey();
		
		else if(piezaEnJuego == peon)
				actualizarPeon();
		
		if(turno==JUGADOR_BLANCAS)
		{
			caminosRey(estado.peligrosReyN, estado.positionRey2);
		
			if(estado.determinarJaque(estado.peligrosReyN))
			{
				MyOptionPane.showMessage("Jaque mate, blancas ganan", "Juego terminado");
				VarsChess.continued=false;
			}
			else estado.determinarReyAhogado(estado.peligrosReyN);
		}
		else
		{
			caminosRey(estado.peligroReyB, estado.positionRey1);
			
			if(estado.determinarJaque(estado.peligroReyB))
			{
				MyOptionPane.showMessage("Jaque mate, negras ganan", "Juego terminado");
				VarsChess.continued=false;
			}
			else estado.determinarReyAhogado(estado.peligroReyB);
		}
		
		if(estado.deleteAction)
			estado.setCaptura(null);
		
	}
	
	private static CasillaData cas;
	
	
	private static void caminosRey(PeligrosPiezas peligros,Position pos)
	{
		peligros.reset();
		
		{
			Amenazas.setActualNonePosition();
			Amenazas.setOnFoundDanger((casilla)->
			{
				peligros.piezasAtacando.insert(null, casilla);
			});
			
			//busca las piezas blancas que dan jaque al rey negro
			Amenazas.buscaPiezas(pos.fila,pos.columna, -turno, false, true,true);
		}
		//Este bloque debe ir despues de haber hecho las busquedas de los jaques del rey
		{
			turno=(byte) -turno;
			VarsChess.rey.buscaCaminos(pos);
			turno=(byte) -turno;
		}
		
		
		//Si hay jaques busca como evitarlo
		if(peligros.piezasAtacando.getSize()==1)
		{
			cas = peligros.piezasAtacando.getNext().value;
			
			{
				Amenazas.setActualNonePosition();
				Amenazas.setOnFoundDanger((casi)->
				{
					if(!(pos.fila == casi.f && pos.columna == casi.c))
					{
						turno=(byte) -turno;
						casi.pieza.validarPieza(casi.f, casi.c);
						turno=(byte) -turno;
						
						if(casi.pieza.indexEv==-1)
							peligros.casillasComeAtaque.insert(null, casi);
					}
				});
				
				//De la posicion que da jaque (Pieza blanca) va a buscar las piezas negras que 
				//pueden atacar a dicha pieza blanca
				Amenazas.buscaPiezas(cas.f, cas.c, turno, false, true,true);
			}
			
			
			
			if(cas.pieza != VarsChess.caballo)
			{
				Amenazas.agregaCasillasAmenaza(pos.fila,pos.columna,cas.f, cas.c,peligros.casillasPasaAmenaza);
			}
			
			if(!peligros.casillasPasaAmenaza.isEmpty())
			{
				while(peligros.casillasPasaAmenaza.hasNext())
				{
					cas = peligros.casillasPasaAmenaza.next().value;
					
					Amenazas.setActualNonePosition();
					Amenazas.setOnFoundDanger((casi)->
					{
						//Agrega como generico <T> la casilla que puede defender
						//Y como parametro <K> la casilla que se puede menear
						if(!(pos.fila == casi.f && pos.columna == casi.c))
						{
							turno=(byte) -turno;
							casi.pieza.validarPieza(casi.f, casi.c);
							turno=(byte) -turno;
							
							if(casi.pieza.indexEv==-1)
								peligros.casillasCubreAtaque.insert(cas, casi);
						}
					});
					
					Amenazas.buscaPiezas(cas.f, cas.c, turno, false, true,false);
				}
				
			}
			
		}
	}
	
	private static void actualizaEnroque()
	{
		
		if(turno == JUGADOR_BLANCAS)
		{
			if(estado.enroqueB)
			{
				if(fromF==tableroAjedrez.filas-1 && fromC==0)
					if(estado.get(tableroAjedrez.filas-1, 2).especialAct!=null)
					{
						estado.deleteActionOf(tableroAjedrez.filas-1, 2);
						estado.actualizarEnroques(true);
					}
					else;
				
				else if(fromF==tableroAjedrez.filas-1 && fromC==7)
					if(estado.get(tableroAjedrez.filas-1, 6).especialAct!=null)
					{
						estado.deleteActionOf(tableroAjedrez.filas-1, 6);
						estado.actualizarEnroques(true);
					}
			}
		}
		else
		{
			if(estado.enroqueN)
			{
				if(fromF==0 && fromC==0)
					if(estado.get(0, 2).especialAct!=null)
					{
						estado.deleteActionOf(0, 2);
						estado.actualizarEnroques(false);
					}
					else;
				
				else if(fromF==0 && fromC==7)
					if(estado.get(0, 6).especialAct!=null)
					{
						estado.deleteActionOf(0, 6);
						estado.actualizarEnroques(false);
					}
				
				
			}
		}
	}
	
	private static void actualizarPeon()
	{
		if(turno==JUGADOR_BLANCAS) 
		{
			if(fromF == tableroAjedrez.filas-2 && toF == tableroAjedrez.filas-4)
			{
				if(estado.isValidPosition(toF, toC-1))
					if(estado.tablero[toF][toC-1].esPeonDe(-JUGADOR_BLANCAS))
					{
						estado.deleteAction=false;
						estado.peonIzq = estado.tablero[toF][toC-1];
						estado.setCaptura
						(estado.tablero[toF+1][toC]);
						
						estado.tablero[toF][toC-1].especialAct = ()->
						{
							//Este es la casilla a la que pasara una negra alcapurar aal paso
							return new Position(toF+1, toC);
						};
						
					}
					else;
				
				if(estado.isValidPosition(toF, toC+1))
					if(estado.tablero[toF][toC+1].esPeonDe(-JUGADOR_BLANCAS))
					{
						estado.deleteAction=false;
						estado.peonDer = estado.tablero[toF][toC+1];
						
						estado.setCaptura
						(estado.tablero[toF+1][toC]);
						
						estado.tablero[toF][toC+1].especialAct = ()->
						{
							return new Position(toF+1, toC);
						};
					}
			}
			else if(estado.tablero[toF][toC] == estado.peonPaso)
			{
				estado.deleteAction=true;
				estado.actualizarEspecial(toF+1, toC, -1, -1, null, (byte) -JUGADOR_BLANCAS);
			}
			else if(toF == 0)
			{
				estado.coronacion(toF, toC,JUGADOR_BLANCAS,
						estado.coronacionBlancas);
			}
		}
		else
		{
			if(fromF == 1 && toF == 3)
			{
				if(estado.isValidPosition(toF, toC-1))
					if(estado.tablero[toF][toC-1].esPeonDe(JUGADOR_BLANCAS))
					{
						estado.deleteAction=false;
						estado.peonIzq = estado.tablero[toF][toC-1];
						estado.setCaptura
						(estado.tablero[toF-1][toC]);
						
						estado.tablero[toF][toC-1].especialAct = ()->
						{
							return new Position(toF-1, toC);
						};
						
					}
					else;
				
				if(estado.isValidPosition(toF, toC+1))
					if(estado.tablero[toF][toC+1].esPeonDe(JUGADOR_BLANCAS))
					{
						estado.deleteAction=false;
						estado.peonIzq = estado.tablero[toF][toC+1];
						estado.setCaptura
						(estado.tablero[toF-1][toC]);
						
						estado.tablero[toF][toC+1].especialAct = ()->
						{
							
							return new Position(toF-1, toC);
						};
					}
			}
			else if(estado.tablero[toF][toC] == estado.peonPaso)
			{
				estado.deleteAction=true;
				estado.actualizarEspecial(toF-1, toC, -1, -1, null, JUGADOR_BLANCAS);
			}
			else if(toF == tableroAjedrez.filas-1) {
				estado.coronacion(toF, toC,JUGADOR_NEGRAS,
						estado.coronacionNegras);
			}
		}
			
	}
	
	private static void actualizarRey()
	{

		if(turno == JUGADOR_BLANCAS)
		{
			if(estado.enroqueB)
			{
				estado.enroqueB = false;
			
				if(estado.get(toF, toC).especialAct!=null)
				{
					estado.get(toF, toC).especialAct.especial_action();
				}
				
				//Estos dos no son necesarios 
				estado.deleteActionOf(tableroAjedrez.filas-1, 2);
				estado.deleteActionOf(tableroAjedrez.filas-1, 6);
			}
			estado.positionRey1.setNewPosition(toF, toC);
		}
		else
		{
			if(estado.enroqueN)
			{
				estado.enroqueN = false;
				
				
				if(estado.get(toF, toC).especialAct!=null)
				{
					estado.get(toF, toC).especialAct.especial_action();
				}
				
				//No so necsarios
				estado.deleteActionOf(0, 2);
				estado.deleteActionOf(0, 6);
			}
			estado.positionRey2.setNewPosition(toF, toC);
		}
	}
}
