package Games.Ajedrez.Perfomance;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


import Conexiones.Constants;
import Games.GameList;
import Games.Ajedrez.Diseno.CasillAjedrez;


import static Games.Ajedrez.Perfomance.VarsChess.continued;
import static Games.Ajedrez.Perfomance.VarsChess.rey;
import static Games.Ajedrez.Perfomance.VarsChess.piezaEnJuego;
import static Games.Ajedrez.Perfomance.VarsChess.turno;
import static Games.Ajedrez.Perfomance.VarsChess.JUGADOR_BLANCAS;

import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;

public class MoveListener extends MouseAdapter implements Constants
{
	private CasillAjedrez casillaTocada,casillaMeneada;
	private byte filaT,columnaT,filaM,columnaM;
	private boolean haEscogido;
	
	
	
	@Override
	public void mouseClicked(MouseEvent e) 
	{
		
		if(!haEscogido)
		{
			casillaTocada = (CasillAjedrez) e.getComponent();
			escoge();
		}
		else
		{
			tableroAjedrez.setCursor(null);
			casillaMeneada =  (CasillAjedrez) e.getComponent();
			filaM = casillaMeneada.fila;
			columnaM = casillaMeneada.columna;
			
			menea();
		}
		
	}
	
	public void menea()
	{
		if(casillaMeneada.isValid)
		{
			casillaMeneada.setIcon(casillaTocada.getIcon());
			casillaTocada.setIcon(null);


			
			Propiedades.copyPropierties(filaT, columnaT, filaM, columnaM);
			
			//Actualiza los movs de las piezas y checa si esta el rey ahogado
			tableroAjedrez.update();
			
			
			if(!tableroAjedrez.modoConexion)
			{
				if(continued)
					tableroAjedrez.onMoved.method();
				
			}else {
					tableroAjedrez.tcp.write(FROM_TO,String.format("%d,%d,%d,%d", filaT,columnaT,filaM,columnaM));
					continued=false;
			}
		}
		
		casillaTocada.defaultColor();
		
		tableroAjedrez.removeCasillas();
		
		haEscogido = false;
	}
	
	public void escoge()
	{
		if(casillaTocada.getIcon()!=null)
		{
			filaT = casillaTocada.fila;
			columnaT = casillaTocada.columna;
			
			if(validarTurno())
			{
				piezaEnJuego = tableroAjedrez.getEstadoActual().get(filaT, columnaT).pieza;
				
				casillaTocada.select();
			
				if(!movEspecial((turno == JUGADOR_BLANCAS) ? tableroAjedrez.getEstadoActual().peligroReyB
						: tableroAjedrez.getEstadoActual().peligrosReyN))
				{
					 tableroAjedrez.getEstadoActual().get(filaT,columnaT).onClick(this::selectCasillas);
					 
				}
				
				haEscogido = true;
			}
		}
	}
	
	private void selectCasillas(CasillaData cas)
	{
		tableroAjedrez.casillasAj[cas.f][cas.c].validateCasilla(true);
		tableroAjedrez.casillasValidas.add(tableroAjedrez.casillasAj[cas.f][cas.c]);
	}
	
	private boolean movEspecial(PeligrosPiezas peligros)
	{
		if(piezaEnJuego == rey)
		{
			casillasRey(peligros.casillasPosibles);
			return true;
		}
		if(peligros.piezasAtacando.getSize()==1)
		{
			CasillaData piezaAtaca = peligros.piezasAtacando.getNext().value;
			
			peligros.casillasCubreAtaque.for_searchBy(this::selectCasillas, 
					tableroAjedrez.getEstadoActual().get(filaT, columnaT));

			peligros.casillasComeAtaque.forEach((node)->
			{
				if(node.value == tableroAjedrez.getEstadoActual().get(filaT, columnaT))
				{
					
					tableroAjedrez.casillasAj[piezaAtaca.f][piezaAtaca.c].validateCasilla(true);
					tableroAjedrez.casillasValidas.add(tableroAjedrez.casillasAj[piezaAtaca.f][piezaAtaca.c]);
					
					peligros.casillasComeAtaque.breakBucle();
				}
			});
			
			return true;
		}
		else if(peligros.piezasAtacando.getSize()>=2)
		{
			return true;
		}
		
		return false;
	}
	
	private void casillasRey(GameList<CasillaData, Void> casillas)
	{
		CasillaData c;
		
		while(casillas.hasNext())
		{
			c = casillas.next().value;
			selectCasillas(c);
		}
		
	}
	
	public boolean validarTurno()
	{
		return continued && tableroAjedrez.getEstadoActual().get(filaT, columnaT).valor == turno;
	}
}
