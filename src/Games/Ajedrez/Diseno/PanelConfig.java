package Games.Ajedrez.Diseno;

import javax.swing.JPanel;

import Games.Abstract.Variables;
import Games.Abstract.VoidMethod;
import Games.Ajedrez.Perfomance.Dificulty;
import Games.Ajedrez.Perfomance.VarsChess;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.SwingConstants;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import static Games.ColorManager.colorClaro;

public class PanelConfig extends JPanel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3015724335469048028L;
	public JLabel label,lblDifculty;
	public boolean escondido=true;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	
	public byte variante;
	public Object modoVS[];
	
	
	private void getProperties(int value)
	{
		for(Dificulty difs : Dificulty.values())
		{
			if(difs.getValue() == value)
			{
				lblDifculty.setText(difs.getName());
				modoVS[1] = difs;
				break;
			}
		}
		
		
	}
	
	public PanelConfig(VoidMethod onClick) 
	{
		variante = VarsChess.TRADICIONAL;
		modoVS = new Object[2];
		modoVS[0] = Variables.JvsJ;
		modoVS[1] = null;
		
		setBackground(colorClaro);
		setLayout(null);
		
		label = new JLabel(Variables.modoChess);
		label.setBounds(0, 49, 52, 51);
		add(label);
		
		JLabel lblTipoAjedrez = new JLabel("Tipo Ajedrez:");
		lblTipoAjedrez.setForeground(Color.BLACK);
		lblTipoAjedrez.setBounds(55, 12, 84, 15);
		add(lblTipoAjedrez);
		
		JRadioButton rdbtnTradicional = new JRadioButton("Tradicional");
		rdbtnTradicional.addItemListener( (e)->{
			
			if(e.getStateChange() == ItemEvent.SELECTED)
				variante = VarsChess.TRADICIONAL;
		});
		
		rdbtnTradicional.setSelected(true);
		buttonGroup.add(rdbtnTradicional);
		rdbtnTradicional.setOpaque(false);
		rdbtnTradicional.setForeground(Color.BLACK);
		rdbtnTradicional.setBounds(55, 35, 90, 23);
		add(rdbtnTradicional);
		
		JRadioButton rdbtnTrascendantal = new JRadioButton("Trascendantal");
		rdbtnTrascendantal.addItemListener((e)->{
			
			if(e.getStateChange() == ItemEvent.SELECTED)
				variante = VarsChess.TRASCENDENTAL;
		});
		buttonGroup.add(rdbtnTrascendantal);
		rdbtnTrascendantal.setForeground(Color.BLACK);
		rdbtnTrascendantal.setOpaque(false);
		rdbtnTrascendantal.setBounds(55, 62, 110, 23);
		add(rdbtnTrascendantal);
		
		JRadioButton rdbtnJubilado = new JRadioButton("Jubilado");
		buttonGroup.add(rdbtnJubilado);
		rdbtnJubilado.addItemListener((e)->{
			
			if(e.getStateChange() == ItemEvent.SELECTED)
				variante = VarsChess.JUBILADO;
		});
		rdbtnJubilado.setOpaque(false);
		rdbtnJubilado.setForeground(Color.BLACK);
		rdbtnJubilado.setBounds(55, 89, 73, 23);
		add(rdbtnJubilado);
		
		JRadioButton rdbtnFsicher = new JRadioButton("Fischer");
		
		rdbtnFsicher.addItemListener((e)->{
			if(e.getStateChange() == ItemEvent.SELECTED)
				variante = VarsChess.ALETORIO_FISCHER;
		});
		
		buttonGroup.add(rdbtnFsicher);
		rdbtnFsicher.setOpaque(false);
		rdbtnFsicher.setForeground(Color.BLACK);
		rdbtnFsicher.setBounds(55, 116, 110, 23);
		add(rdbtnFsicher);
		
		JLabel lblModoJuego = new JLabel("Modo Juego");
		lblModoJuego.setHorizontalAlignment(SwingConstants.CENTER);
		lblModoJuego.setForeground(Color.BLACK);
		lblModoJuego.setBounds(196, 12, 84, 15);
		add(lblModoJuego);
		
		JSlider slider = new JSlider(0,Dificulty.values().length-1,0);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) 
			{
				if(!slider.getValueIsAdjusting())
				{
					getProperties(slider.getValue());
				}
			}
		});
		slider.setEnabled(false);
		slider.setBackground(colorClaro);
		slider.setBounds(212, 97, 134, 23);
		add(slider);
		
		JRadioButton rdbtnJugadorVsJugador = new JRadioButton("Jugador vs Jugador");
		rdbtnJugadorVsJugador.addItemListener((e)->{
			
			if(e.getStateChange() == ItemEvent.SELECTED)
			{
				modoVS[0] = Variables.JvsJ;
				modoVS[1] = null;
				slider.setEnabled(false);
			}
		});
		
		rdbtnJugadorVsJugador.setActionCommand("JJ");
		rdbtnJugadorVsJugador.setSelected(true);
		buttonGroup_1.add(rdbtnJugadorVsJugador);
		rdbtnJugadorVsJugador.setForeground(Color.BLACK);
		rdbtnJugadorVsJugador.setOpaque(false);
		rdbtnJugadorVsJugador.setBounds(206, 35, 140, 23);
		add(rdbtnJugadorVsJugador);
		
		JRadioButton rdbtnJugadorVsPc = new JRadioButton("Jugador vs PC");
		rdbtnJugadorVsPc.addItemListener((e)->
		{
			if(e.getStateChange() == ItemEvent.SELECTED)
			{
				modoVS[0] = Variables.JvsPC;
				getProperties(slider.getValue());
				slider.setEnabled(true);
			}
		});
		
		buttonGroup_1.add(rdbtnJugadorVsPc);
		rdbtnJugadorVsPc.setOpaque(false);
		rdbtnJugadorVsPc.setForeground(Color.BLACK);
		rdbtnJugadorVsPc.setBounds(206, 62, 140, 23);
		add(rdbtnJugadorVsPc);
		
		JLabel lblDificultad = new JLabel("Dificultad");
		lblDificultad.setForeground(Color.BLACK);
		lblDificultad.setHorizontalAlignment(SwingConstants.CENTER);
		lblDificultad.setBounds(396, 12, 90, 15);
		add(lblDificultad);
		
		lblDifculty = new JLabel("Principiante");
		lblDifculty.setHorizontalAlignment(SwingConstants.CENTER);
		lblDifculty.setForeground(Color.BLACK);
		lblDifculty.setBounds(396, 143, 90, 15);
		add(lblDifculty);
		
		
		
		label.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseEntered(MouseEvent e) 
			{
				label.setCursor(Variables.handCursor);
			}
			
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				onClick.method();
				escondido=!escondido;
			}
		});
		
	}
	

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return 520;
	}
	
	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return 170;
	}
}
