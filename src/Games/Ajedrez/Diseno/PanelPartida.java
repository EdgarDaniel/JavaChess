package Games.Ajedrez.Diseno;

import javax.swing.JPanel;
import javax.swing.JSlider;

import Games.Abstract.Variables;
import Games.Abstract.VoidMethod;
import Games.Ajedrez.Perfomance.Pieza;
import Games.Ajedrez.Perfomance.VarsChess;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import static Games.Ajedrez.Diseno.PanelAjedrez.tableroAjedrez;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import static Games.ColorManager.*;

@SuppressWarnings("serial")
public class PanelPartida extends JPanel 
{
	public boolean escondido=true;
	public JLabel label, labelBlancas,labelNegras;
	
	public byte turno = VarsChess.JUGADOR_BLANCAS;;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JToggleButton toggleButtonB,toggleButtonN;
	private JComboBox<String> comboBoxBl,comboBoxNe;
	
	public PanelPartida(VoidMethod onClick) 
	{
		setBackground(colorClaro);
		setLayout(null);
		
		 label = new JLabel(Variables.updateParti);
		label.setBounds(0, 49, 52, 51);
		add(label);
		
		JLabel lblEscogeFichas = new JLabel("Escoge fichas:");
		lblEscogeFichas.setHorizontalAlignment(SwingConstants.CENTER);
		lblEscogeFichas.setForeground(Color.BLACK);
		lblEscogeFichas.setBounds(96, 12, 105, 15);
		add(lblEscogeFichas);
		
		toggleButtonB = new JToggleButton(VarsChess.PEON);
		
		toggleButtonB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				turno = VarsChess.JUGADOR_BLANCAS;
				if(!tableroAjedrez.modoConexion && tableroAjedrez.vsPC && VarsChess.continued)
					tableroAjedrez.pc.updateOptions((byte) -turno).moverPieza();
			}
		});
		toggleButtonB.setSelected(true);
		toggleButtonB.setBackground(colorMedio);
		buttonGroup.add(toggleButtonB);
		toggleButtonB.setBounds(70, 39, 69, 100);
		add(toggleButtonB);
		
		 toggleButtonN = new JToggleButton(VarsChess.PEON2);
		toggleButtonN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				turno = VarsChess.JUGADOR_NEGRAS;

				if(!tableroAjedrez.modoConexion && tableroAjedrez.vsPC && VarsChess.continued)
					tableroAjedrez.pc.updateOptions((byte) -turno).moverPieza();
			}
		});
		toggleButtonN.setBackground(colorMedio);
		buttonGroup.add(toggleButtonN);
		toggleButtonN.setBounds(151, 39, 69, 100);
		add(toggleButtonN);
		
		comboBoxBl = new JComboBox<String>();
		comboBoxBl.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) 
			{
				if(arg0.getStateChange() == ItemEvent.SELECTED)
				{
					Pieza p = getCoronacion(comboBoxBl.getSelectedItem().toString());
					
					tableroAjedrez.getEstadoActual().coronacionBlancas = p;
					labelBlancas.setIcon(p.getPieza(VarsChess.JUGADOR_BLANCAS));
					
				}
			}
		});
		comboBoxBl.setModel(new DefaultComboBoxModel<String>(new String[] {"Reina", "Caballo", "Torre", "Alfil"}));
		comboBoxBl.setBounds(261, 39, 105, 24);
		add(comboBoxBl);
		
		comboBoxNe = new JComboBox<>();
		comboBoxNe.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				
				if(arg0.getStateChange() == ItemEvent.SELECTED)
				{
					
					Pieza p =  getCoronacion(comboBoxNe.getSelectedItem().toString());
					
					tableroAjedrez.getEstadoActual().coronacionNegras = p;
					labelNegras.setIcon(p.getPieza(VarsChess.JUGADOR_NEGRAS));
				}
			}
		});
		comboBoxNe.setModel(new DefaultComboBoxModel<String>(new String[] {"Reina", "Caballo", "Torre", "Alfil"}));
		comboBoxNe.setBounds(390, 39, 105, 24);
		add(comboBoxNe);
		
		labelBlancas = new JLabel(VarsChess.REINA);
		labelBlancas.setBounds(283, 75, 64, 64);
		add(labelBlancas);
		
		labelNegras = new JLabel(VarsChess.REINA2);
		labelNegras.setBounds(412, 75, 64, 64);
		add(labelNegras);
		
		label.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseEntered(MouseEvent e) 
			{
				label.setCursor(Variables.handCursor);
			}
			
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				onClick.method();
				escondido=!escondido;
			}
		});

	}

	public void habiltarNegras(boolean enab)
	{
//		toggleButtonN.setEnabled(enab);
		comboBoxNe.setEnabled(enab);
		toggleButtonB.setSelected(!enab);
	}
	
	public void habiltarBlancas(boolean enab)
	{
//		toggleButtonB.setEnabled(enab);
		comboBoxBl.setEnabled(enab);
		toggleButtonN.setSelected(!enab);
	}
	
	private Pieza getCoronacion(String piece)
	{
		switch(piece)
		{
			case "Reina":
				return VarsChess.reina;
				
			case "Alfil":
				return VarsChess.alfil;
						
			case "Caballo":
				return VarsChess.caballo;
				
			case "Torre":
				return VarsChess.torre;
				
		}
		
		return null;
	}
	
	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return 520;
	}
	
	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return 170;
	}
}
