package Games.Ajedrez.Diseno;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.Timer;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;

import Games.Animador;
import Games.MyOptionPane;
import Games.Abstract.PanelConnect;
import Games.Abstract.Variables;
import Games.Abstract.VoidMethod;

import static Games.ColorManager.*;

public class PanelAjedrez extends JPanel implements ActionListener
{

	private static final long serialVersionUID = -2802525308470741997L;
	
	public static TableroAjedrez tableroAjedrez;
	public static PanelPartida panelPartida;
	
	private static Timer notificacion;
	private static String msg,title;
	
	private JTextArea textArea;
	
	private JPanel panel,panel_1;
	private Component comp;
	
	private boolean block=false;
	public static JLabel lblConexion;
	
	private PanelConnect panelConecction;
	private PanelConfig panelConfig;
	
//	private static boolean recibido;
	private boolean enMov;
	
	private int MAX_SPACEX;
	
	private ComponentClicked cc;
	
	private Animador animador;
	
	private static VoidMethod accepted,canceled;
	
	
	public PanelAjedrez() 
	{
		
		animador = new Animador(30,this::requestFocus);
		notificacion = new Timer(500, this);
		
		setPreferredSize(new Dimension(683, 723));
		
		setForeground(colorClaro);
		setBackground(colorFuerte);
		setLayout(null);
		
		panelConecction = new PanelConnect(this::animacionPanelConnect);
//		panelConecction.setBounds(getWidth()-50, 50, panelConecction.getWidth(), panelConecction.getHeight());
		add(panelConecction);
		
		panelConfig = new PanelConfig(this::animacionPanelConfig);
//		panelConfig.setBounds(getWidth()-50,panelConecction.getHeight()+50+30, panelConfig.getWidth(), 
//				panelConfig.getHeight());
		add(panelConfig);
		
		panelPartida = new PanelPartida(this::animacionPanelPartida);
		add(panelPartida);

		
		panel = new JPanel();
		panel.setBounds(20, 29, 584, 559);
		panel.setBorder(new MatteBorder(4, 4, 4, 4, (Color) new Color(0, 0, 0)));
		panel.setBackground(colorFuerte);
		panel.setLayout(null);
		add(panel);
		
		textArea = new JTextArea();
		textArea.setDragEnabled(true);
		
		tableroAjedrez = new TableroAjedrez((byte)8,(byte) 8,colorClaro, colorMedio);
		tableroAjedrez.setBounds(25, 12, 533, 533);
		panel.add(tableroAjedrez);
		
		panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel_1.setBackground(colorFuerte);
		panel_1.setBounds(20, 600, 584, 42);
		add(panel_1);
		panel_1.setLayout(null);
		
		JButton btnJuegoNuevo = new JButton("Juego Nuevo");
		
		btnJuegoNuevo.addActionListener((e)->
		{
			tableroAjedrez.juegoNuevo(panelConfig.variante,panelConfig.modoVS,panelPartida.turno);
		});
		
		btnJuegoNuevo.setBounds(12, 8, 110,25);
		btnJuegoNuevo.setBackground(colorClaro);
		panel_1.add(btnJuegoNuevo);
		
		JButton btnGuardarPartida = new JButton("Guardar Partida");
		btnGuardarPartida.setEnabled(false);
		btnGuardarPartida.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
//				tableroAjedrez.guardarJuego(new File("chessgame"));
			}
		});
		btnGuardarPartida.setBackground(colorClaro);
		btnGuardarPartida.setBounds(127, 8, 129, 25);
		panel_1.add(btnGuardarPartida);
		
		JButton btnGuardarJugadas = new JButton("Guardar Jugadas");
		btnGuardarJugadas.setEnabled(false);
		btnGuardarJugadas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
//				tableroAjedrez.guardarJugadas(new File(""));
			}
		});
		btnGuardarJugadas.setBackground(colorClaro);
		btnGuardarJugadas.setBounds(260, 8, 147, 25);
		panel_1.add(btnGuardarJugadas);

		JButton btnCargarPartida = new JButton("Cargar Partida");
		btnCargarPartida.setEnabled(false);
		btnCargarPartida.setMnemonic('C');
		btnCargarPartida.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
//				tableroAjedrez.cargarJuego(new File("chessgame"));
			}
		});
		btnCargarPartida.setBackground(colorClaro);
		btnCargarPartida.setBounds(412, 8, 147, 25);
		panel_1.add(btnCargarPartida);
		
		lblConexion = new JLabel(Variables.conected);
		lblConexion.setHorizontalAlignment(JLabel.CENTER);
		lblConexion.setEnabled(false);
		add(lblConexion);
		
		addListeners();
		
		
//		tableroAjedrez.juegoNuevo(VarsChess.TRADICIONAL,new Object[]{Variables.JvsJ},panelPartida.turno);
	}
	
	private void animacionPanelPartida()
	{
		if(panelPartida.escondido)
		{
			animador.setFunction(panelPartida,MAX_SPACEX, -50, 0, getWidth(), 
					panelConfig.getBounds().y+panelConfig.getHeight()+30, panelPartida.getWidth(), panelPartida.getHeight());
		}
		else
		{
			animador.setFunction(panelPartida, getWidth()-50, 50, 0, panelPartida.getBounds().x, 
					panelPartida.getBounds().y, panelPartida.getWidth(), panelPartida.getHeight());
		}
		
		animador.play();
		panelPartida.label.setCursor(null);
	}
	
	private void animacionPanelConfig()
	{
		if(panelConfig.escondido)
		{
			animador.setFunction(panelConfig,MAX_SPACEX, -50, 0, getWidth(), 
					panelConecction.getHeight()+80, panelConfig.getWidth(), panelConfig.getHeight());
		}
		else
		{
			animador.setFunction(panelConfig, getWidth()-50, 50, 0, panelConfig.getBounds().x, 
					panelConfig.getBounds().y, panelConfig.getWidth(), panelConfig.getHeight());
		}
		
		animador.play();
		panelConfig.label.setCursor(null);
	}
	
	private void animacionPanelConnect()
	{
		if(panelConecction.escondido)
		{
			
			animador.setFunction(panelConecction, 
					MAX_SPACEX, -50, 0, getWidth(), 55, panelConecction.getWidth(), 
					panelConecction.getHeight());
		}
		else 
		{
			animador.setFunction(panelConecction, getWidth()-50, 50, 0, 
					panelConecction.getBounds().x, panelConecction.getBounds().y, 
					panelConecction.getWidth(), panelConecction.getHeight());
		}
		
		animador.play();
		panelConecction.label.setCursor(null);
		
	}
	
	private void addListeners()
	{
		lblConexion.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				if(lblConexion.isEnabled())
				{
					if(notificacion.isRunning())
					{
						notificacion.stop();
						notificar();
					}
					else none();
				}
			}
		});
		
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e)
			{
				if(e.isControlDown())
					enMov=true;
				
				else if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
				{
					cancelDragg();
					enMov= false;
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) 
			{
				if(enMov)
					enMov=false;
			}
			
		});
		setFocusable(true);
		
		addMouseMotionListener(new MouseMotionAdapter() 
		{
			@Override
			public void mouseDragged(MouseEvent e) 
			{
				if(comp!=null)
				{
					if(e.getX()+comp.getBounds().width<=MAX_SPACEX-15)
					{
						comp.setBounds(e.getX(),e.getY(),comp.getBounds().width,comp.getBounds().height);
						block = false;
					}else block=true;
					repaint();
				}
			}
		});
		
		addComponentListener(new ComponentAdapter() 
		{
			public void componentResized(ComponentEvent e) 
			{
				panelConecction.setBounds(getWidth()-50, 50, panelConecction.getWidth(), panelConecction.getHeight());
				panelConfig.setBounds(getWidth()-50,panelConecction.getHeight()+80, panelConfig.getWidth(), 
						panelConfig.getHeight());
				panelPartida.setBounds(getWidth()-50,panelConfig.getBounds().y+panelConfig.getHeight()+30,
						panelPartida.getWidth(),
						panelPartida.getHeight());
				
				panelConecction.escondido=true;
				panelConfig.escondido=true;
				
				lblConexion.setBounds(getWidth()-35, 5, 34, 34);
				
				MAX_SPACEX = getWidth()-540;
			}
		});
		
		cc = new ComponentClicked();
		
		panel_1.addMouseListener(cc);
		panel.addMouseListener(cc);
		
	}
	
	private static boolean x;
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		x=!x;
		lblConexion.setIcon(x ? null : Variables.conected);
	}
	
	
	private void notificar()
	{
		lblConexion.setIcon(Variables.conected);
		
		int val = MyOptionPane.showConfirm(msg,title);
		
		if(val == JOptionPane.OK_OPTION)
		{
			accepted.method();
			
		}else
		{
			canceled.method();
		}
	}
	
	private void none()
	{
		MyOptionPane.showMessage("No hay notificaciones", "Solicitudes");
	}
	
	public static void notificacion(String ms,String ti,VoidMethod onConfirm,VoidMethod onCancel)
	{
		msg = ms;
		title = ti;
		
		accepted = onConfirm;
		canceled = onCancel;
		
		x=true;
		notificacion.start();
		lblConexion.setEnabled(true);
		lblConexion.setCursor(Variables.handCursor);
		
	}
	
	@Override
	protected void paintComponent(Graphics arg0) 
	{
		super.paintComponent(arg0);
		
		if(block)
		{
			arg0.setColor(Color.red);
			arg0.drawLine(MAX_SPACEX-15, 0, MAX_SPACEX-10, getHeight());
		}
	}
	
	private void cancelDragg()
	{
		comp = null;
		setCursor(null);
		block = false;
		repaint();
	}
	
	class ComponentClicked extends MouseAdapter
	{
		@Override
		public void mousePressed(MouseEvent e) 
		{
			if(enMov)
			{
				comp = e.getComponent();
				setCursor(Variables.moveCursor);
			}
			else {
				cancelDragg();
			}
		}
	}
	
	
}

