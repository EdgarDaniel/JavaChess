package Games.Ajedrez.Diseno;

import static Games.Abstract.Variables.MODO_SERVER;
import static Games.Abstract.Variables.assertIJFor;
import static Games.Abstract.Variables.convertArray;
import static Games.Abstract.Variables.iFor;
import static Games.Abstract.Variables.jFor;
import static Games.Abstract.Variables.reserIJ;
import static Games.Ajedrez.Perfomance.VarsChess.ALETORIO_FISCHER;
import static Games.Ajedrez.Perfomance.VarsChess.JUBILADO;
import static Games.Ajedrez.Perfomance.VarsChess.JUGADOR_BLANCAS;
import static Games.Ajedrez.Perfomance.VarsChess.JUGADOR_NEGRAS;
import static Games.Ajedrez.Perfomance.VarsChess.TRADICIONAL;
import static Games.Ajedrez.Perfomance.VarsChess.TRASCENDENTAL;
import static Games.Ajedrez.Perfomance.VarsChess.alfil;
import static Games.Ajedrez.Perfomance.VarsChess.caballo;
import static Games.Ajedrez.Perfomance.VarsChess.continued;
import static Games.Ajedrez.Perfomance.VarsChess.k;
import static Games.Ajedrez.Perfomance.VarsChess.l;
import static Games.Ajedrez.Perfomance.VarsChess.peon;
import static Games.Ajedrez.Perfomance.VarsChess.piezaEnJuego;
import static Games.Ajedrez.Perfomance.VarsChess.reina;
import static Games.Ajedrez.Perfomance.VarsChess.rey;
import static Games.Ajedrez.Perfomance.VarsChess.torre;
import static Games.Ajedrez.Perfomance.VarsChess.turno;


import java.awt.Color;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.swing.Timer;

import Conexiones.Action;
import Conexiones.ConectionListener;
import Conexiones.Constants;
import Conexiones.net.ConexionLocal;
import Conexiones.net.TCP;
import Games.MyOptionPane;
import Games.Principal;
import Games.Abstract.CasillaDataAbstract;
import Games.Abstract.Tablero;
import Games.Abstract.Variables;
import Games.Abstract.VoidMethod;
import Games.Ajedrez.Perfomance.CasillaData;
import Games.Ajedrez.Perfomance.DatosAjedrez;
import Games.Ajedrez.Perfomance.Dificulty;
import Games.Ajedrez.Perfomance.EstadoTablero;
import Games.Ajedrez.Perfomance.GuardaJugadas;
import Games.Ajedrez.Perfomance.MoveListener;
import Games.Ajedrez.Perfomance.PC;
import Games.Ajedrez.Perfomance.Pieza;
import Games.Ajedrez.Perfomance.Propiedades;
import Games.Ajedrez.Perfomance.VarsChess;
import Manejador.DragDropListener;
import Manejador.Serializa;
@SuppressWarnings("serial")
public class TableroAjedrez extends Tablero implements Action,ConectionListener, Constants, DragDropListener
{
		
		public CasillAjedrez casillasAj[][];
		private EstadoTablero estadoAux,estado;
		public GuardaJugadas jugadas;
		public  PC pc;
		
		
		public ConexionLocal conexion;
		public TCP tcp;
		public byte movs[];
		public boolean modoConexion;
		
		public VoidMethod onMoved;
		
		public  MoveListener listener;
		public Color selectColor,marckColor;
		public byte ID_GAME = 1;
		
		public ArrayList<CasillAjedrez> casillasValidas;
		
		public byte modoJuego,varianteJuego;
		private Object modoVS[];
		private String ruta;
		
		public TableroAjedrez(byte filas, byte columnas,Color c1,Color c2) 
		{
			super(filas, columnas,c1,c2);
			
			estado = new EstadoTablero(filas,columnas);
			selectColor = Color.green;
			marckColor = Color.yellow;
			pc = new PC(estado);
			
			rey.methodRey = ()->
			{
				if(turno==JUGADOR_BLANCAS)
				{
					estado.peligroReyB.casillasPosibles.insert(null, estado.tablero[k][l]);
				}
				else {
					estado.peligrosReyN.casillasPosibles.insert(null, estado.tablero[k][l]);
				}
			};
			
			addDraggDropListener(this);
//			TODO init turno
			turno = JUGADOR_BLANCAS;
			pc.updateOptions(JUGADOR_NEGRAS);
		}

		@Override
		public void onConected() 
		{
			PanelAjedrez.lblConexion.setEnabled(true);
			continued=false;
		}
		
		@Override
		public void onDesconected() 
		{
			PanelAjedrez.lblConexion.setEnabled(false);
			continued=false;
		}
		
		
		@Override
		public boolean connect(byte modoJ) 
		{
			if(conexion==null)
				conexion = new ConexionLocal("localhost", 1234);
			
			tcp = conexion.get(modoJ==MODO_SERVER,this,this,Principal.instance,null);

			
			if(tcp.connect(conexion.host,conexion.puerto))
			{
				modoJuego=modoJ;
				return modoConexion = true;
			}
			modoJuego=Variables.MODO_NORMAL;
			return modoConexion=false;
		}
		
		@Override
		public void action(int key, Object code) 
		{
			switch(key)
			{
				case FROM_TO:
					continued=true;
					String mov = (String)code;
					
					movs = convertArray(mov.split(","));
					
					piezaEnJuego = estado.get(movs[0], movs[1]).pieza;
					
					casillasAj[movs[2]][movs[3]].setIcon(casillasAj[movs[0]][movs[1]].getIcon());
					casillasAj[movs[0]][movs[1]].setIcon(null);
					
					turno=(byte) -turno;
					Propiedades.copyPropierties(movs[0], movs[1], movs[2], movs[3]);
					update();
					turno=(byte) -turno;
					break;
					
				case CHANGE_PROMOTION:
//					PanelAjedrez.changeCoronacion(estado.modoJuego==MODO_CLIENT,code);
					
					break;
					
				case NOTIFICACION:
					Object data[] = (Object[])code;
					PanelAjedrez.notificacion(data[0].toString(), data[1].toString(),this::aceptado,this::rechazado);
					varianteJuego = (byte) data[2];
					modoVS = (Object[]) data[3];
					turno = (byte) data[4];
					break;
					
				case ACEPTADO:
					MyOptionPane.showMessage("Se ha aceptado la solicitud","---");
					checkVS();
					initGame();
					break;
					
				case RECHAZADO:
					MyOptionPane.showMessage("Ha sido rechazada la solicitud","---");
					break;
//				case FROM:
//					mov = (String)code;
//					movPeon = false;
//					movs = convertArray(mov.split(","));
//					
//					estado.actualizarTablero(movs[0], movs[1],turno);
//					casillasAj[movs[0]][movs[1]].setIcon(null);
//					break;
					
//				case CODIFIC:
//					Codificador codificador = (Codificador) code;
//					descodificar(codificador.casillas);
//					break;
					
//				case TRADICIONAL:
//					ajedrezTradicional();
//					break;
//					
//				case JUBILADO:
//					ajedrezJubilado();
//					break;
			}
		}
		
		public EstadoTablero getEstadoActual()
		{
			return estadoAux;
		}
		
		public void setEstadoTo(boolean normal)
		{
				estadoAux = normal ? estado : pc.estado;
		}
		
		//Se debe actualizar tablero
		private void descodificar(CasillaData casillas[][])
		{
			estado.tablero = casillas;
			assertIJFor();
			
			CasillaDataAbstract cd[][] = new CasillaDataAbstract[casillas.length][casillas[0].length];
			for(; iFor<casillas.length; iFor++)
			{
				for(jFor=0; jFor<casillas[iFor].length; jFor++)
				{
					asignarPieza(casillas[iFor][jFor].casillaName);
					casillasAj[iFor][jFor].putPiece(casillas[iFor][jFor].pieza, casillas[iFor][jFor].valor);
				}
			}
			reserIJ();
			
			jugadas = new GuardaJugadas(cd, super.firstColor, super.secondColor);
		}
		
		private void asignarPieza(String casillaName)
		{
			switch(casillaName)
			{
				case "PB":
				case "PN":
					estado.tablero[iFor][jFor].pieza = peon;
					break;
					
				case "TB":
				case "TN":
					estado.tablero[iFor][jFor].pieza = torre;
					break;
				
				case "CB":
				case "CN":
					estado.tablero[iFor][jFor].pieza = caballo;
					break;

				case "AB":
				case "AN":
					estado.tablero[iFor][jFor].pieza = alfil;
					break;
					
				case "DB":
				case "DN":
					estado.tablero[iFor][jFor].pieza = reina;
					break;
					
				case "RB":
				case "RN":
					estado.tablero[iFor][jFor].pieza = rey;
					break;
			}
		}
		
		private Pieza piezas [] = {torre,caballo, alfil, reina,rey,alfil,caballo,torre};
		
		private void ajedrezTradicional()
		{
			Pieza piezas[] = {torre,caballo, alfil,reina,rey,alfil,caballo,torre};
			CasillaDataAbstract cda[][] = new CasillaDataAbstract[filas][columnas];

			for (int j=0; j<piezas.length; j++)
			{
				
				casillasAj[0][j].putPiece(piezas[j],JUGADOR_NEGRAS);
				estado.actualizarTablero(0,j,piezas[j],JUGADOR_NEGRAS);
				cda[0][j] = new CasillaDataAbstract(JUGADOR_NEGRAS, piezas[j]);
				
				
				casillasAj[super.filas-1][j].putPiece(piezas[j],JUGADOR_BLANCAS);
				estado.actualizarTablero(super.filas-1,j,piezas[j],JUGADOR_BLANCAS);
				cda[super.filas-1][j] = new CasillaDataAbstract(JUGADOR_BLANCAS, piezas[j]);
				
				casillasAj[1][j].putPiece(peon, JUGADOR_NEGRAS);
				estado.actualizarTablero(1,j,peon,JUGADOR_NEGRAS);
				cda[1][j] = new CasillaDataAbstract(JUGADOR_NEGRAS, peon);
				
				casillasAj[super.filas-2][j].putPiece(peon,JUGADOR_BLANCAS);
				estado.actualizarTablero(super.filas-2,j,peon,JUGADOR_BLANCAS);
				cda[super.filas-2][j] = new CasillaDataAbstract(JUGADOR_BLANCAS, peon);
			}
			estado.positionRey1.setNewPosition(super.filas-1, 4);
			estado.positionRey2.setNewPosition(0, 4);
//			
			jugadas = new GuardaJugadas(cda, super.firstColor, super.secondColor);
			
		}
		
		private void ajedrezAletorioDe(boolean fischer)
		{
			Collections.shuffle(Arrays.asList(this.piezas));
			int j;
			
			
			for (j=0; j<piezas.length; j++)
			{
				
				casillasAj[0][j].putPiece(piezas[j],JUGADOR_NEGRAS);
				estado.actualizarTablero(0,j,piezas[j],JUGADOR_NEGRAS);
				
				if(piezas[j]==rey)
					estado.positionRey2.setNewPosition(0, j);
				
				casillasAj[1][j].putPiece(peon, JUGADOR_NEGRAS);
				estado.actualizarTablero(1,j,peon,JUGADOR_NEGRAS);
				
				casillasAj[6][j].putPiece(peon,JUGADOR_BLANCAS);
				estado.actualizarTablero(6,j,peon,JUGADOR_BLANCAS);
			}
			
			if(!fischer)
				Collections.shuffle(Arrays.asList(this.piezas));
			
			for(j=0; j<piezas.length; j++)
			{
				casillasAj[7][j].putPiece(piezas[j],JUGADOR_BLANCAS);
				estado.actualizarTablero(7,j,piezas[j],JUGADOR_BLANCAS);
				
				if(piezas[j]==rey)
					estado.positionRey1.setNewPosition(7, j);
			}
			
			
		}
		
		private void ajedrezJubilado()
		{
			for (int j=0; j<columnas; j++)
			{
				casillasAj[1][j].putPiece(peon, JUGADOR_NEGRAS);
				estado.actualizarTablero(1,j,peon,JUGADOR_NEGRAS);
				
				casillasAj[6][j].putPiece(peon,JUGADOR_BLANCAS);
				estado.actualizarTablero(6,j,peon,JUGADOR_BLANCAS);
				
				if(j==4)
				{
				casillasAj[0][4].putPiece(rey, JUGADOR_NEGRAS);
				estado.actualizarTablero(0,4,rey,JUGADOR_NEGRAS);
				
				casillasAj[7][4].putPiece(rey,JUGADOR_BLANCAS);
				estado.actualizarTablero(7,4,rey,JUGADOR_BLANCAS);
				}
				else
				{
					casillasAj[0][j].setIcon(null);
					estado.actualizarTablero(0,j,JUGADOR_NEGRAS);
					
					casillasAj[7][j].setIcon(null);
					estado.actualizarTablero(7,j,JUGADOR_BLANCAS);
					
				}
			}
			estado.positionRey1.setNewPosition(7, 4);
			estado.positionRey2.setNewPosition(0, 4);
			
			
		}
		
		@Override
		public CasillAjedrez getCasilla(int f, int c) 
		{
			return casillasAj[f][c];
		}

		@Override
		public void crearAtributos() 
		{
			casillasAj = new CasillAjedrez[super.filas][super.columnas];
			listener = new MoveListener();
			casillasValidas = new ArrayList<CasillAjedrez>(25);
		}

		@Override
		public void instanciar(byte i, byte j) 
		{
			casillasAj[i][j] = new CasillAjedrez(i, j, pintado ? firstColor : secondColor);
			casillasAj[i][j].addMouseListener(listener);
			super.add(casillasAj[i][j]);
			
		}
		
		private boolean first=true;
		public boolean vsPC;
		@Override
		public void juegoNuevo(byte varJ,Object modoVS[],byte turn) 
		{
			//TODO Juego nuevo
			varianteJuego = varJ;
			this.modoVS = modoVS;
			turno = turn;
			checkVS();
			
			if(modoConexion) 
			{
				
				tcp.write(NOTIFICACION, new Object[]
				{
						String.format("Variante juego: %s\nModo vs: %s\nFichas rival: %s", 
							getValorVariante(),getValorVS(), turno == JUGADOR_BLANCAS ? "Blancas" : "Negras"),
						"Partida Ajedrez Solicitada",
						varianteJuego,
						modoVS,
						(byte)-turno,
				});
			}
			
			else
			{
				initGame();
			}			
		}
		
		private void checkVS()
		{
			if((byte)modoVS[0] == Variables.JvsJ)
			{
				onMoved = ()-> turno = (byte) -turno;
				vsPC = false;
				
				if(!modoConexion)
					turno = JUGADOR_BLANCAS;
			}
				
				else 
				{
					onMoved=pc::start;
					vsPC=true;
					pc.updateDificulty((Dificulty)modoVS[1]);
					pc.updateOptions((byte) -turno);
				}
		}
		
		private void initGame()
		{
			if(!first)
				reset();
			
			first = false;
			tipoJuegos();
			
			continued = turno == JUGADOR_BLANCAS;
			
			if(modoConexion)
				if (continued)
				{
					PanelAjedrez.panelPartida.habiltarNegras(false);
					PanelAjedrez.panelPartida.habiltarBlancas(true);
				}
				else 
				{
					PanelAjedrez.panelPartida.habiltarNegras(true);
					PanelAjedrez.panelPartida.habiltarBlancas(false);
				}
			else
			{
				if(!continued) {
					onMoved.method();
				}
					
			}
				
		}
		
		private void aceptado()
		{
			tcp.write(ACEPTADO, null);
			initGame();
		}
		
		private void rechazado()
		{
			tcp.write(RECHAZADO, null);			
		}
		
		
		
		private void tipoJuegos()
		{
			setEstadoTo(true);
			
			estado.reseteando = true;
			switch(varianteJuego)
			{
				case TRADICIONAL:
					ajedrezTradicional();
					break;
					
//				case ALETORIO_FISCHER:
//						if(listener.modoConexion = (modo == CSERVER))
//							if(!connect())
//								return;
//							
//						ajedrezAletorioDe(true);
//					break;
//					
//				case TRASCENDENTAL:
//						if(listener.modoConexion = (modo == CSERVER))
//							if(!connect())
//								return;
//							
//						ajedrezAletorioDe(false);
//					break;
					
				case JUBILADO:
					ajedrezJubilado();
					break;
			}
			estado.reseteando = false;
			update();
			
		}
		
		public  void update()
		{
			byte turn = turno;
			
			getEstadoActual().hayMovs=false;
			
			turno = JUGADOR_BLANCAS;
			getEstadoActual().checarMovs = turn == JUGADOR_NEGRAS;
			for(CasillaData cp : getEstadoActual().piezasBlancas)
				cp.actualizarMovs();
			
			getEstadoActual().checarMovs = turn == JUGADOR_BLANCAS;
			turno = JUGADOR_NEGRAS;
			for (CasillaData cp : getEstadoActual().piezasNegras)
				cp.actualizarMovs();
			
			if(turn == JUGADOR_BLANCAS)
				if(getEstadoActual().determinarReyAhogado(getEstadoActual().peligrosReyN))
				{
					MyOptionPane.showMessage("Rey negro ahogado", "Tablas");
					VarsChess.continued=false;
				}
				else;
			else
				if(getEstadoActual().determinarReyAhogado(getEstadoActual().peligroReyB))
				{
					MyOptionPane.showMessage("Rey blanco ahogado", "Tablas");
					VarsChess.continued=false;
				}
			
//			for (CasillaData cp : estado.piezasNegras)
//			{
//				System.out.println("Movs de pN "+cp);
//				System.out.println(cp.movimientos);
//			}
//			
//			for (CasillaData cp : estado.piezasBlancas)
//			{
//				System.out.println("Movs de pB "+cp);
//				System.out.println(cp.movimientos);
//			}
			
			turno = turn;
		}
		
		@Override
		public void reset() 
		{
			assertIJFor();
			removeCasillas();
			estado.reset();
			
			for(iFor=2; iFor<=5; iFor++)
			{
				for(jFor=0; jFor<columnas; jFor++)
				{
					casillasAj[iFor][jFor].setIcon(null);
					estado.actualizarTablero(iFor, jFor,(byte)0);
				}
			}
			
			jugadas.lista.removeAll();
			casillasValidas.clear();
			
			reserIJ();
			
		}
		
		@Override
		public void cargarJuego(File file) 
		{
			first = false;
			System.out.println("Entrando");
			DatosAjedrez data = (DatosAjedrez) Serializa.writeObject(file);
			estado = data.estado;
			descodificar(estado.tablero);
			pc.estado = estado;
			modoConexion = false;
			continued=true;
			setEstadoTo(true);
			varianteJuego = data.varianteJuego;
			turno =data.turno;
			//modoVS = data.modoVS;
			modoVS = new Object[] {Variables.JvsPC,Dificulty.FACIL};
			checkVS();
			update();
			
			piezaEnJuego = estado.get(4, 4).pieza;
			Propiedades.copyPropierties((byte)4, (byte)4, (byte)3, (byte)3);
			casillasAj[3][3].setIcon(casillasAj[4][4].getIcon());
			casillasAj[4][4].setIcon(null);
			
			update();
			onMoved.method();
//			juegoNuevo(data.varianteJuego, data.modoVS, data.turno);
			
			estado.coronacionBlancas = VarsChess.reina;
			estado.coronacionNegras = VarsChess.reina;
			
		}
			
		
		@Override
		public void guardarJuego(File file) 
		{
			// TODO Auto-generated method stub
			Serializa.saveObject(new DatosAjedrez(turno, varianteJuego, modoVS, modoJuego, estado), file);
		}
		
		@Override
		public void guardarJugadas(File file) 
		{
			Serializa.saveObject(jugadas, file);
		}
		
		public void busquedasClick()
		{
			casillasAj[k][l].validateCasilla(true);
			
			casillasValidas.add(casillasAj[k][l]);
		}
		
		public void removeCasillas()
		{
			casillasValidas.forEach((c)-> c.validateCasilla(false));
			
			casillasValidas.clear();
		}

		@Override
		public void onDragEntered(DataFlavor[] data,Transferable tr,DropTargetDragEvent dtde) throws UnsupportedFlavorException, IOException 
		{
			ruta=null;
			if(data[1].isFlavorTextType())
			{
				
				DataFlavor selectedFlavor = DataFlavor.selectBestTextFlavor(data);
				
				if(tr.getTransferData(selectedFlavor) instanceof Reader)
				{				
					BufferedReader bf = new BufferedReader(((Reader) tr.getTransferData(selectedFlavor)));
					int size=0;
					String c;
					
					while((c=bf.readLine())!=null)
					{
						size++;
						
						if(size==2)
						{
							dtde.rejectDrag();
							ruta=null;
							return;
						}
						ruta = c;
					}
					
					int ind = ruta.lastIndexOf(".")+1;
					String ext = ruta.substring(ind,ruta.length()).trim();
					
					if(validExtension(ext))
					{
						ruta = ruta.replace("file://", "").trim();
					}
					else {
						dtde.rejectDrag();
						ruta = null;
					}
				}
				else dtde.rejectDrag();
			}
		}

		@Override
		public void onDragExited() {
			// TODO Auto-generated method stub
		}
		
		@Override
		public boolean validExtension(String ext) 
		{
			return ext.equalsIgnoreCase("jcvg");
		}

		@Override
		public void onDrop(DropTargetDropEvent dvt) throws UnsupportedFlavorException, 
										IOException 
		{
			dvt.acceptDrop( DnDConstants.ACTION_COPY );
			
			if(ruta!=null)
				cargarJuego(new File(ruta));
			
            dvt.dropComplete(true);
		}

		private String getValorVS()
		{
			String modo = Byte.valueOf(modoVS[0].toString()) == Variables.JvsJ ?
					"Jugador vs Jugador" : "Jugador vs PC";
			
			if(modoVS[1]==null)
			{
				return modo;
			}
			
			String dif = ((Dificulty)modoVS[1]).getName();
			
			return modo+"\nDificultad: "+dif;
		}
		
		private String getValorVariante()
		{
			switch(varianteJuego)
			{
				case TRADICIONAL:
					return "Tradicional";
					
				case JUBILADO:
					return "Jubilado";
					
				case TRASCENDENTAL:
					return "Trascendental";
							
				case ALETORIO_FISCHER:
					return "De Fischer";
			}
			
			return "Desconocido";
		}
	
}