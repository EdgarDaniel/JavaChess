package Games.Ajedrez.Diseno;

import java.awt.Color;
import java.awt.Dimension;

import Games.Abstract.Casilla;
import Games.Abstract.Variables;
import Games.Ajedrez.Perfomance.Pieza;


public class CasillAjedrez extends Casilla
{
	private static final long serialVersionUID = -3493775696764233208L;

	public CasillAjedrez(byte f, byte c,Color cl) 
	{
		super(f,c,cl);
		setPreferredSize(new Dimension(64,64));
	}

	public void putPiece(Pieza pieza, byte player) 
	{
		setIcon(pieza == null ? null : pieza.getPieza(player));
	}
	
	@Override
	public void select() 
	{
		setBackground(PanelAjedrez.tableroAjedrez.selectColor);
		
	}
	
	@Override
	public void marck() 
	{
		setBackground(PanelAjedrez.tableroAjedrez.marckColor);
		setCursor(Variables.select);
	}
	
	@Override
	public void defaultColor() {
		// TODO Auto-generated method stub
		super.defaultColor();
		setCursor(null);
	}
	
}
