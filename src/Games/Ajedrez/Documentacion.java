package Games.Ajedrez;

public class Documentacion 
{
	/**
	 *  La variable "Pieza.metodo" es usada para indicarle al programa que debe hacer al encontrar 
	 *  una casilla valida/disponible para la pieza
	 *  
	 *  Dicha variable puede tomar como metodos:
	 *  	1) TableroAjedrez.busquedasClick = Este se encarga de marcar una casilla como valida y darle un 
	 *  		color, para indicar que ahi se puede menear
	 *  	
	 *  	2) Pc.agregarCasillaValida = Este metodo se encargara solo de añadir a una lista
	 *  	las casillas a las que se puede menear tal pieza, sin marcarlas como valida.
	 *  	Esto con el fin  de ver los movs posibles de todas las piezas enemigas y analizarlas
	 * 
	 *  
	 *  Amenazas.hayAmenazas: Este metodo sirve para determinar si alguna pieza al moverse a tal casilla
	 *  se pone en peligro por una pieza enemiga/amiga. El parametro que recibe brea
	 *  
	 *  
	 *  Amenazas.breakOnFound: Este metodo usa la variable Boolean.method para asignar
	 *  unica y obligatoriamente un return true, para asi a la hora de que el metodo
	 *  busca amenazas encuentra al menos una pieza enemiga de inmediato termine de buscar, sin 
	 *  importar si hay dos piezas enemigas o mas que pueden atacar.
	 *  
	 *  Amenazas.setOnfFoundDanger(metodo): Este metodo es la contrario del breaOnfFound,
	 *  ya que dentro de el la variable Boolean.method debe obligatorioamente return false,para si indicar 
	 *  que busque todas las piezas que te pueden atacar al posicionarse en cietra casilla
	 *  Ademas el metodo debe contener la lista de piezas que te atacan en esa posicion.
	 *  
	 *  Tablero.getEstadActual: Este metodo retornar el EstadoTablero al que se estudiara,
	 *  ya sea el original del juego, o el de la PC, el cual se encargara de ejecutar algoritss
	 *  sin afectar el estado del tablero principal
	 *  
	 */
}
