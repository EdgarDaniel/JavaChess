package Games;

import java.io.Serializable;
import java.lang.annotation.Documented;
import java.util.Random;

import Games.Abstract.ParameterMethod;

import static java.lang.System.out;

public class GameList<T,K> implements Serializable,Cloneable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 632056962032930124L;


	private NodeGame<T,K> inicio,fin,aux,puntero;
	
	private int size;
	
	public void insert(K d,T datos)
	{
		if(inicio==null)
			aux = inicio = fin = new NodeGame<T,K>(d,datos);
		else {
			NodeGame<T,K> aux;
			aux = fin;
			fin.next = new NodeGame<T,K>(d,datos);
			fin = fin.next;
			fin.before = aux; 
		}
		size++;
	}
	
	public boolean hasNext()
	{
		if(aux==null)
		{
			aux = inicio;
			return false;
		}
		
		return true;
	}
	
	public NodeGame<T,K> before() throws NullPointerException
	{
		if(puntero.before!=null)
		{
			aux = puntero;
			puntero = puntero.before;
		}		
		
		else throw new NullPointerException("Ya no hay mas elementos hacia atras");
		
		return puntero;
	}
	
	//Saca todos los valores que esten deespues de puntero;
	public void popNexts()
	{
		puntero.next=null;
		fin = puntero;
	}
	
	public void for_searchBy(ParameterMethod<K> metodo, T elementSearch)
	{
		NodeGame<T, K> ref;
		
		while(hasNext())
		{
			ref = next();
			
			if(ref.value == elementSearch)
			{
				metodo.get(ref.key);
			}
		}
	}
	
	public void forEach(ParameterMethod<NodeGame<T, K>> metodo)
	{
		while(hasNext())
		{
			metodo.get(next());
		}
	}
	
	public boolean existsBoth(T valor, T key)
	{
		NodeGame<T, K> node;
		
		while(hasNext())
		{
			node = next();
			
			if(node.value == valor && key == node.key)
				return true;
		}
		
		return false;
	}
	
	public void breakBucle()
	{
		aux = null;
	}
	
	public NodeGame<T,K> next() throws NullPointerException
	{
		if(aux!=null)
		{
			puntero = aux;
			aux = aux.next;
		}
		
		
		else throw new NullPointerException("Ya no hay mas elementos hacia adelante");
		
		return puntero;
	}
	
	public NodeGame<T,K> getNext()
	{
		return aux;
	}
	
	public void removeAll()
	{
		size=0;
		inicio = fin = puntero = aux = null;
	}
	

	
	public int getSize() {
		return size;
	}
	
	public boolean isEmpty()
	{
		return size==0;
	}
	
	public NodeGame<T, K> getRandom()
	{
		Random lb = new Random();
		
		int ind = lb.nextInt(size);
		
		NodeGame<T, K> nodo=inicio;
		
		for(int i=0; i<ind; nodo=nodo.next, i++);
		return nodo;
	}
	
	public GameList<T, K> copy()
	{
		GameList<T, K> g = new GameList<T,K>();
		
		while (hasNext()) 
		{
			NodeGame<T, K> nodo = next();
			g.insert(nodo.key, nodo.value);
		}
		
		return g;
	}
	
	@Override
	public String toString() 
	{
		String cad="";
		for(NodeGame<T, K> nods = inicio; nods!=null; cad+=nods,nods=nods.next);
		
		return cad;
	}
	
}
