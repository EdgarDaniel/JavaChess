**About the Game**

This is an old Chess Game written in Java. I developed this Chess a few years ago.
For me, this the best personal/hobby project I've developed, since it was a 
challenge to develop a functional Chess Game.
This game can be played as: 

    > Player vs Player on same game
    
    
    > Player vs Player on same machine  (two Java Chess Games running in the same machine connected as TCP/IP localhost)
    
    
    > Player vs PC on same machine
    
    
    > Player vs Player on different machines but in the same LAN
    
    
    > Chosse between black or white pieces.
    
    
    > You can choose what piece you'd like to be take when a pawn is promoted.

Note: The option **"Player vs PC"** is not smart enough, since it doesn't use AI or heuristic/minmax algorithms.

The Game has other features such chossing an specific Chess Game but for now, only the 'Traditional' can be played.
Also, the game can save/load the a chess game successfully but it only save/load the same file
as well as this feature has some bugs.

---

**Planning Features**

I haven't continued this project for a long time, but the game be played without any problem.
As for my C learning progress, I'm planning for my free times, to develop a Chess engine in this
language (and possibly create another Chess engine but written in Rust).

For this Chess C Engine, the logic for all the moves for every chess piece will be written
in this language and for the implementation (the chessboard GUI) could be written in
any language (either Web Based App or Desktop App).
To achive the communication between the Web/Desktop Based App and the Chess C Engine
I will implemente the following communication mechanisms, such as:

    > Shared Memory (POSIX and SysV)
    
    
    > UNIX DOMAIN SOCKETS
    
    
    > TCP/IP

---

**How to run/play**

The game can be easily launched using the file "Principal.java"

To start a new Game (player vs player) just click on button: "Juevo Nuevo" and click on piece you want to move and if this one is able to move to another
position then the possible squares will be iluminated (background changes to yellow).